---
title: tm-lottie lottie动画
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/lottie/index"></mobile-tm>

<content-tm>

# tm-lottie lottie动画

:::tip 提醒

本组件<Badge text="1.2.0" />版本新增。

---
下方动画的静态演示资源地址为本站地址，可能随时失效，请不要在项目中直接引用我的静态资源。谢谢你的配合！

:::

:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:使用方法,跟数1,2,3一样简单
---

**示例**

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<template>
<tm-fullView>
		<template  v-slot:default="{ info }">
			<view class="grey text " :class="tmVuetify.black ? 'grey-darken-5 bk' : ''"
				:style="{ minHeight: info.height + 'px' }">
	<tm-menubars title="lottie动画" color="bg-gradient-blue-accent" :showback="true"></tm-menubars>
	<view class="flex-top-center">
		<tm-lottie :height="650" ref='lottie' url="https://jx2d.cn/yuwuimages/lottie/test.json"></tm-lottie>
	</view>
	<tm-sheet :shadow="24">
		<tm-button @click="$refs.lottie.play()" :shadow="0" size="m" theme="grey" text>播放</tm-button>
		<tm-button @click="$refs.lottie.pause()" :shadow="0" size="m" theme="grey" text>暂停</tm-button>
		<tm-button @click="$refs.lottie.stop()" :shadow="0" size="m" theme="grey" text>停止</tm-button>
		<tm-button @click="$refs.lottie.setDirection(-1)" :shadow="0" size="m" theme="grey" text>反向播放</tm-button>
		<tm-button @click="$refs.lottie.setDirection(1)" :shadow="0" size="m" theme="grey" text>顺序播放</tm-button>
		<tm-menu @change="reloadAni" direction="top" :black="true" :list="['官方logo动画','测试人物动画','数据空动画','搜索空','爱情']">
			<tm-button icon="icon-sort-down" size="m" :shadow="0" theme="blue" text>选择其它动画示例</tm-button>
		</tm-menu>
		
		<view class="py-32 text-size-s text-grey">
			<view :class="tmVuetify.black ? ' bk' : ''" class="py-24 border-b-1 border-t-1">说明：如果弹出的菜单被动画盖住属于正常，因为原生级别高于我的组件，不影响你选择动画示例。</view>
			<view class="text-blue pt-24">了解和制作：http://airbnb.io/lottie</view>
			<view  class="text-blue">免费动画下载：https://lottiefiles.com/</view>
		</view>
	</tm-sheet>
	</view>
	</template>
		
</tm-fullView>
		
</template>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	
	export default {
		data() {
			return {
				jsonList:[
					'https://jx2d.cn/yuwuimages/lottie/logo.json',
					'https://jx2d.cn/yuwuimages/lottie/test.json',
					'https://jx2d.cn/yuwuimages/lottie/empty.json',
					'https://jx2d.cn/yuwuimages/lottie/search-empty.json',
					'https://jx2d.cn/yuwuimages/lottie/love.json',
				]
			};
		},
		async onReady() {
			
		},
		methods: {
			async reloadAni(e){
				await this.$refs.lottie.registerAnimation(this.jsonList[e.index])
			},
			
		},
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>



</content-tm>


<content-tm>

### :point_right:props属性表

| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| width | Number | 任意整数 | 420 | 画布宽度 |
| height | Number | 任意整数 | 420 | 画布高度 |
| url | String | lottie动画json数据或者链接地址 | '' | 地址或者json数据 |
| loop | Boolean | true/false | true | 循环播放 |
| autoplay | Boolean | true/false | true | 自动播放 |

**url的说明见下方**

</content-tm>

<content-tm>

### :point_right:ref方法

| 方法名称 | 所需参数 | 返参 | 说明 |
| :-----: | :--: | :---: | :---: |
| play | - | - | 播放动画 |
| stop | - | - | 停止动画 |
| pause | - | 暂停动画 |
| setDirection | (1/-1) | - | 动画播放方向1正向，-1反向 |
| registerAnimation | (url/json) | - | 加载一个新的动画，url为json数据或者可以返回json数据链接的地址,也可直接传json数据 |

:::tip 特别提醒

因为小程序限制和其它的兼容。统一规定：<br>
url:如果是链接，那么url请看我的demo演示，是指请求url返回json数据具体返回格式为{code:0,data:{你的json}}<br>
如果你想提供本地json数据，那么请把url设置为空。然后使用ref函数）：<br>
registerAnimation(json数据)<br>
json数据是你本地读取后调用registerAnimation(你的数据)执行，而不是提供本地json地址！！！<br>

**如果你要问我，怎么读取本地json数据，那么请你左拐，我不送**

:::

</content-tm>



<content-tm>

### :point_right:更新日志

**2021年8月20日14:8:5**
初始版本。


</content-tm>

<testtime> </testtime>