---
title: tm-loadding 加载状态
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/com/loadding"></mobile-tm>

<content-tm>

# tm-loadding 加载状态
:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:使用方法,跟数1,2,3一样简单
---

**基本示例**

```vue
<tm-loadding></tm-loadding>
```

**加载成功**
```vue
<tm-loadding :success="true"></tm-loadding>
```

**加载失败**
```vue
<tm-loadding :fail="true"></tm-loadding>
```
**自定义值** <Badge text="1.1.9" />
```vue
<tm-loadding success icon="icon-times-circle-fill" label="没有更多啦" color="blue"></tm-loadding>
```

:::tip 小技巧

pros属性中优先级顺序为<br>
fail > success > load

:::

因此三个属性可以一起放，就设置那个为true即可，方便加载文档
<br>
比如下面的代码，只会显示加载成功了，不会显示加载中.
```vue
<!-- 只显示 加载失败 不显示加载中 -->
<tm-loadding :load="true" :fail="true"></tm-loadding>
```

</content-tm>


<content-tm>

### :point_right:props属性表

**三个参数同时被设置时**，优先级顺序：```fail > success > load```
| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| load | Boolean | true/false | true | 显示加载中 |
| fail | Boolean | true/false | false | 显示加载失败 |
| success | Boolean | true/false | false | 显示加载成功 |
| label<Badge text="1.1.9" /> | String | - | '' | 自定义文本 |
| icon<Badge text="1.1.9" /> | String | - | '' | 自定义图标 |
| color<Badge text="1.1.9" /> | String | - | '' | 自定义主题 |


</content-tm>

<content-tm>

### :point_right:事件

| 事件名称 | 返回参数 | 返参类型 | 说明 |
| :-----: | :--: | :---: | :---: |
| @click | - | - | 点击加载区域时触发 |



</content-tm>





<content-tm>

### :point_right:更新日志

**2021年8月20日14:8:5**
初始版本。


</content-tm>

<testtime> </testtime>