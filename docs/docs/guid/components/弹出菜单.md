---
title: tm-menu 弹出菜单
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/com/menu"></mobile-tm>

<content-tm>

# tm-menu 弹出菜单
:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:使用方法,跟数1,2,3一样简单
---

**基本示例**

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<tm-menu :list="['发起群聊','添加朋友']">
	<tm-button>居中显示</tm-button>
</tm-menu>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
			}
		},
		methods: {}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>



</content-tm>



<content-tm>

### :point_right:完整的示例
---

**可以复制到页面直接运行**
<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<template>
	<view>
		<tm-menubars title="弹出菜单" color="bg-gradient-blue-accent" :showback="true"></tm-menubars>
		<tm-sheet color="blue text ">
			<tm-menu :list="['发起群聊','添加朋友']">
				<tm-button>居中显示</tm-button>
			</tm-menu>
			<tm-menu black :list="list">
				<tm-button>图标列表显示</tm-button>
			</tm-menu>
		</tm-sheet>
		<tm-sheet color="blue text ">
			<tm-menu direction="top" :list="['发起群聊','添加朋友']">
				<tm-button>上方显示</tm-button>
			</tm-menu>
			<tm-menu direction="top" black :list="list">
				<tm-button>图标列表显示</tm-button>
			</tm-menu>
		</tm-sheet>
		<tm-sheet color="blue text ">
			
			<tm-menu direction="bottom" ment-direction="left" tip-direction="left" black :list="list">
				<view style="width: 200upx;">底部左边</view>
			</tm-menu>
			<tm-menu  black :list="list">
				<view style="width: 200upx;" class="text-align-center">底部居中</view>
			</tm-menu>
			<tm-menu direction="bottom" ment-direction="right" tip-direction="right"  black :list="list">
				<view style="width: 200upx;">底部右边</view>
			</tm-menu>
		</tm-sheet>
		<tm-sheet color="blue text ">
			<tm-menu show direction="bottom" ment-direction="right"  tip-direction="right"  black :list="list">
				<view class="d-inline-block">默认显示底部，左边指示三角形在右<tm-icons name="icon-plus-circle-fill"></tm-icons>边  </view>
			</tm-menu>
		</tm-sheet>
	</view>
</template>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
				list:[
					{title:'发起群聊',icon:'icon-pengyouquan',iconColor:'green'},
					{title:'添加朋友',icon:'icon-QQ',iconColor:'blue'}
				],
				show:true
			}
		},
		methods: {
			
		}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>

</content-tm>


<content-tm>

### :point_right:props属性表

| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| tip-direction | String | left/center/right | center | 指示三角形的方向，小三角也可以，左中右对齐  |
| direction | String | top/bottom |  bottom | 内容框竖向的弹出方向，向上和向下弹出 |
| ment-direction | String | left/center/right |  center | 内容框水平方向的对齐方式，左中右对齐 |
| rang-key | String | - |  title | 菜单列表数组对象时，需要提供key |
| icon-color | String | 任意主题色名称 |  primary | 统一配置列表图标颜色，如果数据格式中提供，则优先使用数据中的颜色值 |
| black | Boolean | true/false | false | 暗黑模式 |
| disabled | Boolean | true/false | false | 是否禁用 |
| maxHeight | Number | 任意整数 | 500 | 最大高度，超过变成滚动菜单内容。单位rpx |
| show | Boolean | true/false | false | 显示菜单，用于默认显示的功能，如需双向绑定使用show.sync |
| list | Array | - | [] | 菜单列表可以是字符串对象或者数组对象 |

**list数据格式**
可以是字符串数组
```vue
<tm-menu :list="['发起群聊','添加朋友']"><tm-button>居中显示</tm-button></tm-menu>
```
也可是对象：[{...}],对象时格式如下。如果显示需要提供rang-key
```js
[
	{
		title:'发起群聊',//必须。此时的range-key 为title.需要在pros中将rang-key="title"
		icon:'icon-pengyouquan',//可不提供，图标名称或者图片地址
		iconColor:'green' //图标颜色，可不提供
	}
]

```

</content-tm>

<content-tm>

### :point_right:事件

| 事件名称 | 返回参数 | 返参类型 | 说明 |
| :-----: | :--: | :---: | :---: |
| @change | {index:列表索引,value:项目数据} | Object | 点击列表项目时触发 |

```vue
<tm-menu @change="change" :list="['发起群聊','添加朋友']"><tm-button>居中显示</tm-button></tm-menu>
```
```js
change(e){
	// 此时为{index:0,value:"发起群聊"}
	console.log(e);
}

```
</content-tm>


<content-tm>

### :point_right:更新日志

**2021年8月20日14:8:5**
初始版本。


</content-tm>

<testtime> </testtime>