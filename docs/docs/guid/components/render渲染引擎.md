---
title: tm-render render渲染引擎
---
<mobile-tm src="https://jx2d.cn/uniapp/#/pages/index/crender"></mobile-tm>

<content-tm>

# tm-render render渲染引擎

:::tip 提醒

本组件<Badge text="1.2.27" />版本新增。

---

详见文档[render渲染](/guid/render/介绍.md)

:::

:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:使用方法,跟数1,2,3一样简单
---

**示例**

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue

<tm-render  ref='render' @render="ok"></tm-render>

```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	import tmRender from '@/tm-vuetify/components/tm-render/tm-render.vue';
	import tmButton from '@/tm-vuetify/components/tm-button/tm-button.vue';
	import ring from '@/tm-vuetify/tool/function/crender/graph/ring'

	let graph = null;
	export default {
		components: {
			tmRender,tmButton
		},
		data() {
			return {

			};
		},
		async mounted() {
			
		},
		methods: {
			async mousedown() {

			},
			async ok(e) {
				// 渲染完成。
				console.log('渲染完成');
				const [w, h] = e;
				let t = this;
				let rectWidth = 100;
				let rectHeight = 60;
				let crender = this.$refs.render.getRender();
				this.$refs.render.addGraph(ring(crender))
			},
			wait(time=500) {
			  return new Promise(resolve => setTimeout(resolve, time))
			},
			async testkkk() {
				let crender = this.$refs.render.getRender();
				crender.delAllGraph();
				this.$refs.render.addGraph(ring(crender))
			},
			async addrect(){
				let crender = this.$refs.render.getRender();
				crender.delAllGraph();
				const [w,h] = crender.area;
				let graph =await this.$refs.render.addGraph({
					name: 'rect',
					animationCurve: 'liner',
					hover: false,
					drag: false,
					animationFrame:50,
					shape: {
						x:0,
						y:(h-50)/2,
						w:100,
						h:50
					},
					style: {
						fill:'#f00'
					}
				})
				
				graph.animation("shape",{x:w-100},true,true)
				await graph.animation("style",{fill:'green'})
				await uni.$tm.sleep(200)
				graph.animation("style",{fill:'#f00'},true)
				await graph.animation("shape",{x:(w-100)/2})
				await graph.animation("shape",{w:0,h:0})
				await crender.delGraph(graph)
				graph =await this.$refs.render.addGraph({
					name: 'circle',
					animationCurve: 'liner',
					hover: false,
					drag: false,
					animationFrame:40,
					shape: {
						rx:w/2,
						ry:h/2,
						r:0
					},
					style: {
						fill:'#f00'
					}
				})
				await graph.animation("shape",{r:50})
				
			}
		},
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>



</content-tm>


<content-tm>

### :point_right:props属性表

| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| width | Number | 任意整数 | 0 | 画布宽度,默认父级宽度 |
| height | Number | 任意整数 | 600 | 画布高度,单位rpx |


</content-tm>

<content-tm>

### :point_right:ref方法

| 方法名称 | 所需参数 | 返参 | 说明 |
| :-----: | :--: | :---: | :---: |
| getRender | - | - | 返回渲染render对象 |
| addGraph | graph | object | 添加图形元素 |

**render和graph对象的方法见其官方文档，[crender](http://crender.jiaminghi.com/)**

</content-tm>



<content-tm>

### :point_right:更新日志

**2021年8月20日14:8:5**
初始版本。


</content-tm>

<testtime> </testtime>