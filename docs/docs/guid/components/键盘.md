---
title: tm-keyborad 键盘
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/com/keyborad"></mobile-tm>

<content-tm>

# tm-keyborad 键盘
:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:使用方法,跟数1,2,3一样简单
---

**基本示例**

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<tm-sheet>
	<tm-input title-class="text-weight-b text-blue" title="点击输入显示数字键盘" :vertical="true" @click="show=true" v-model="word"></tm-input>
	<tm-keyborad  model="number" :show.sync="show" v-model="word"></tm-keyborad>
</tm-sheet>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
				word:'',
				show:true,
			}
		},
		methods: {}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>

</content-tm>

<content-tm>

### :point_right:数字、身份证、车牌、密码键盘
---

:::tip 提示
键盘一共分为四类<br>
数字键盘（小数点和没有小数点），身份证键盘，车牌键盘，密码键盘，四类型，切换Props model属性即可啦。
:::

**完整的示例**

以下可以复制直接运行哦。不同的键盘类型```注意高亮部分```

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue{7,11,15,19}
<template>
	<view>
		<tm-menubars title="tm-tranlate" color="bg-gradient-blue-accent"  :showback="true"></tm-menubars>
		
		<tm-sheet>
			<tm-input title-class="text-weight-b text-blue" title="点击输入显示数字键盘" :vertical="true" @click="show=true" v-model="word"></tm-input>
			<tm-keyborad :black="black" model="number" :show.sync="show" v-model="word"></tm-keyborad>
		</tm-sheet>
		<tm-sheet>
			<tm-button @click="show1=true">身份证键盘</tm-button> <tm-button @click="black=true;show1=true">暗黑模式</tm-button>
			<tm-keyborad :black="black" model="code" :show.sync="show1" v-model="word"></tm-keyborad>
		</tm-sheet>
		<tm-sheet>
			<tm-button @click="show2=true">车牌键盘</tm-button>
			<tm-keyborad :black="black" model="car" :show.sync="show2" v-model="word"></tm-keyborad>
		</tm-sheet>
		<tm-sheet>
			<tm-button @click="show3=true">密码键盘</tm-button>
			<tm-keyborad :black="black" model="password" :show.sync="show3" v-model="word"></tm-keyborad>
		</tm-sheet>
	</view>
</template>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
				word:'',
				show:true,
				show1:false,
				show2:false,
				show3:false,
				black:false
			}
		},
		methods: {
			
		}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>

</content-tm>


<content-tm>

### :point_right:启用暗黑模式
---
只要```:black="true"```即可啦
```vue{3}
<!-- 完整的示例见上方 -->
<tm-sheet>
	<tm-keyborad :black="true"></tm-keyborad>
</tm-sheet>
```
</content-tm>


<content-tm>

### :point_right:props属性表

:::warning 提醒
在暗黑模式下color属性将失效。
:::

| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| value | String|Number | 任意文本 | '' | 输入的数据，推荐使用v-model,也可使用value.sync双向绑定到输入框. |
| show.sync | Boolean | true/false | false | 显示键盘，请使用show.sync双向绑定 |
| color | String | 任意主题色名 | white | 通用按钮主题色,除了特定的按钮 |
| title | String | 任意文本 | 安全键盘放心输入 | 键盘顶部标题文字 |
| ok-color | String | 任意主题色名 | primary |  确认按钮的主题色 |
| round | Number | 任意整数 | 2 |  按钮圆角，单位upx |
| shadown | Number | 任意整数 | 5 |  按钮按钮投影，单位upx |
| black | Boolean | true/false | false |  暗黑模式 |
| decimal | Boolean | true/false | true |  是否显示小数点 |
| model | String | 见下方 | 'number' |  键盘类型数字键盘 |

**键盘类型model的可选值**

| number | code | car | password |
| :-----: | :--: | :---: | :---: |
| 数字键盘 | 身份证键盘 | 车牌键盘 | 密码键盘 |

</content-tm>

<content-tm>

### :point_right:事件

| 事件名称 | 返回参数 | 返参类型 | 说明 |
| :-----: | :--: | :---: | :---: |
| @confirm | input | String | 键盘上点击确认键触发，并携带该键盘输入的数据，同v-model数据一致 |

示例如下
<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue{2}
<tm-keyborad 
@confirm="confirm"  
model="number" :show.sync="show" v-model="word"></tm-keyborad>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
				word:'',
				show:true,
			}
		},
		methods: {
			confirm(val){
				console.log(`我是输入的数据：${val}`);
			}
		}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>


</content-tm>





<content-tm>

### :point_right:更新日志

**2021年8月20日14:8:5**
初始版本。


</content-tm>

<testtime> </testtime>