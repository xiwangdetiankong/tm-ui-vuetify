---
title: tm-teps 步骤条
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/com/teps"></mobile-tm>

<content-tm>

# tm-teps 步骤条

:::warning 注意

1.1.4版本后（含），不兼容旧版本的颜色属性。新版本进行了重构，变得和其它组件统一，更为美观和精致。

:::

:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:使用方法,跟数1,2,3一样简单
---

**基本示例**

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<tm-steps model="icon" v-model="active1" :list="list2" rangeKey="text"></tm-steps>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
				active1:0,
				list2:[
					{text:'步骤1'},
					{text:'步骤2'},
					{text:'步骤3'},
				],
			}
		},
		methods: {}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>

**也可以使用字符串数组**

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<tm-steps model="number" v-model="active1" :list="list2"></tm-steps>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
				active1:0,
				list2:['提交资料','审核资料','复核','成功提交'],
			}
		},
		methods: {}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>

</content-tm>



<content-tm>

### :point_right:完整的示例
---

**可以复制到页面直接运行**
<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<template>
	<tm-fullView>
		<template v-slot:default="{info}">
			<view class="grey text " :class="tmVuetify.black?'black bk':''" :style="{minHeight:info.height+'px'}">
				<tm-menubars title="步骤条" color="bg-gradient-blue-accent" :showback="true"></tm-menubars>
				<tm-sheet :shadow="24">
					<view class="pa-12 text-size-s text-weight-b blue text mb-32 round-2" :class="tmVuetify.black?'black bk':''">基础示例</view>
					<tm-steps model="number" color="bg-gradient-blue-accent" activeFontColor="blue" line-color="bg-gradient-blue-accent" v-model="active" :list="list"></tm-steps>
					<view class="pa-12 text-size-s text-weight-b blue text mb-32 round-2" :class="tmVuetify.black?'black bk':''">通过插槽改变不同步骤的图标</view>
					<tm-steps model="icon" color="bg-gradient-pink-accent" active-icon="icon-angle-right" icon="icon-sync-alt" activeFontColor="pink" line-color="bg-gradient-pink-accent" v-model="active" :list="list">
						<template v-slot:icon="{data}">
							<block v-if="data.active&&data.index==2">
								<tm-icons :size="22" name="icon-times" color="yellow"></tm-icons>
							</block>
							<block v-if="!data.active">
								<tm-icons :size="22" :name="data.icon" color="grey"></tm-icons>
							</block>
							<block v-if="data.active&&data.index!=2">
								<tm-icons :size="22" :name="data.activeIcon" color="white"></tm-icons>
							</block>
						</template>
						
					</tm-steps>
					<tm-button theme="bg-gradient-blue-accent"    @click="active+=1">下一步</tm-button>
					<tm-button theme="grey-lighten-4"  font-color="blue" :shadow="0" @click="active-=1">上一步</tm-button>
				</tm-sheet>
				<tm-sheet :shadow="24">
					<view class="pa-12 text-size-s text-weight-b blue text mb-32 round-2" :class="tmVuetify.black?'black bk':''">点击步骤条</view>
					<tm-steps @click="iclick" model="icon" v-model="active1" :list="list2" rangeKey="text"></tm-steps>
					
				</tm-sheet>
			
				<view style="height: 50upx;"></view>
			</view>
		</template>
	</tm-fullView>

</template>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
				active:2,
				active1:0,
				list:['提交资料','审核资料','复核','成功提交'],
				list2:[
					{text:'步骤1'},
					{text:'步骤2'},
					{text:'步骤3'},
				],
			}
		},
		methods: {
			iclick(e){
				uni.$tm.toast(`你点击了${e}`)
				this.active1 = e;
			}
		}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>

</content-tm>


<content-tm>

### :point_right:props属性表
:::danger 注意
版本1.1.4后已经被重构颜色值只能是主题色名称。
:::

| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| value.sync | Number | 任意数字 | 0 | 当前激活的项。双向绑定使用value.sync或者v-model，步骤是从1开始。 |
| black | Boolean | true/false | false | 暗黑模式 |
| color | String | 任意主题名称 | primary | 主题颜色激活的背景色。 |
| range-key | String | - | text | 数据为对象数组时，需要提供对应的文字key |
| size | Number | - | 42 | 节点大小。 |
| line-style | css的边线类型 | - | solid | 标准的css边线类型，比如:实线solid,虚线dashed |
| line-width | Number | - | 1 | 连线的厚度,单位rpx |
| line-in-icon | 图标名称 | - | '' | 如果填写了图标名称，则在节点连间内的中间显示图标。 |
| line-color | String | 任意主题名称 | primary | 激活时的线条颜色值 |
| font-color | String | 任意主题名称 | grey | 默认未激活时的文字主题色 |
| active-icon | String | 图标名称 | icon-check | 激活后的图标 |
| icon | String | 图标名称 | icon-clock-fill | 未激活的图标 |
| active-font-color | String | 颜任意主题名称 | primary | 默认激活时的文字主题色 |
| model | String | number/icon | number | 两种模式，number:数字，icon:图标模式 |


</content-tm>

<content-tm>

### :point_right:事件

| 事件名称 | 返回参数 | 返参类型 | 说明 |
| :-----: | :--: | :---: | :---: |
| @change | 当前步骤位置 | Number | 步骤变换时触发 |
| @click | 步骤位置 | Number | 点击步骤节点时触发 |



</content-tm>

<content-tm>

### :point_right:slot插槽

**使用方法见完整示例**

| 插槽名称 | 插槽参数 | 插槽数据 | 说明 |
| :-----: | :--: | :---: | :---: |
| icon | data | {icon:未激活图标,activeIcon:激活图标,index:当前索引1开始,active:当前是否激活} | 图标模式的插槽 |
| number | data | {icon:未激活图标,activeIcon:激活图标,index:当前索引1开始,active:当前是否激活} | 数字模式的插槽 |
| default| data | 项目数据 | 底下文字的插槽 |



</content-tm>

<content-tm>

### :point_right:更新日志

**2021年9月6日**
此组件已被重构，已统一颜色属性为主题名称，不再支持颜色的16进制值。

**2021年8月20日14:8:5**
初始版本。


</content-tm>

<testtime> </testtime>