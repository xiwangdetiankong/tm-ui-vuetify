---
title: translate 动画
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/com/translate"></mobile-tm>

<content-tm>

# tm-translate 动画组件
:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:基本示例
---
默认自动播放
```vue

<tm-translate  animation-name="fadeIn">
	<tm-button theme="blue">渐隐进</tm-button>
</tm-translate>

```

</content-tm>

<content-tm>

### :point_right:禁止自动播放

```vue{2}

<tm-translate :auto="false" animation-name="fadeIn">
	<tm-button theme="blue">渐隐进</tm-button>
</tm-translate>

```

</content-tm>

<content-tm>

### :point_right:手动播放

通过```$refs```可以使用```play() stop()```两个函数。

```vue{2}

<tm-translate ref="a_1" :auto="false" animation-name="fadeIn">
	<tm-button @click="$refs.a_1.play()" theme="blue">渐隐进</tm-button>
</tm-translate>

```

</content-tm>

<content-tm>

### :point_right:props属性表

| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| duration | Number | 任意整数 | 500 | 动画时长 |
| wait | Number | 任意整数 | 0 | 延时多少毫秒开始播放 |
| auto | String\|Boolean | true\|false | true | 是否自动播放动画 |
|  animation-name | String | fadeUp\|fadeDown\|fadeLeft<br>fadeRight\|zoomIn\|zoomOut<br>fadeIn\|fadeOut | fadeUp | 动画效果名称 |

**animation-name可选值说明**

| fadeUp|fadeDown|fadeLeft|fadeRight|zoomIn|zoomOut|fadeIn|fadeOut |
| :-----: | :--: | :---: | :---: | :--- | :--- | :--- | :---: |
| 下至上 |上至下|右至左|左至右|大至小|正常至无|渐显|渐隐 |

</content-tm>

<content-tm>

### :point_right:事件

| 事件名称 | 返回参数 | 返参类型 | 说明 |
| :-----: | :--: | :---: | :---: |
| @start | 无 | 无 | 动画开始执行触发 |
| @end | 无 | 无 | 动画结束执行触发 |


</content-tm>

<content-tm>

### :point_right: ref组件可用函数


<CodeGroup>
  <CodeGroupItem title="vue">

```vue{1}
<tm-translate ref="a_1" :auto="false" animation-name="fadeIn">
</tm-translate>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" active>

```js
//需要通过$refs.具名来执行
//播放，
this.$refs.a_1.play()
//停止
this.$refs.a_1.stop()停止
```

  </CodeGroupItem>
</CodeGroup>


| 函数名 | 返回参数 | 返参类型 | 说明 |
| :-----: | :--: | :---: | :---: |
| play | 无 | 无 | 播放 |
| stop | 无 | 无 | 停止 |


</content-tm>

<content-tm>

### :point_right:更新日志

**2021年8月20日14:8:5**
初始版本。


</content-tm>

<testtime> </testtime>