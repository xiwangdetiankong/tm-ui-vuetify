---
title: tm-menubars 菜单工具栏
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/com/menubars"></mobile-tm>

<content-tm>

# tm-menubars 菜单工具栏

:::tip 提醒
此菜单顶部工具栏，集成了最基础的导航菜单。以及下拉渐变背景。如果需要个性化的导航栏，需要自行使用插槽去设计。
<br>
我后续会增加一些基于此封装好的个性常见菜单工具栏。

:::

:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:使用方法,跟数1,2,3一样简单
---

```vue
<tm-menubars title="菜单工具栏" iconColor="white" ></tm-menubars>
```



</content-tm>






<content-tm>

### :point_right:props属性表

| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| scroll-tobg | Number | 任意数字 | 0 | 当值小于当前值即开启透明背景。下拉时达到设定的值，即显示自定义的背景和文字色 |
| color | String | 任意主题色名称 | primary | 导航栏背景主题颜色名称 |
| fontColor | String | 任意主题色名称 | null | 导航栏文字的颜色，注意：如果你提供了，主题自带的推理文字颜色将失效 |
| theme | String | black/white/custom | custom | 只有在transparent:true时，black:表示黑色模式，文字变白。white文字变黑。 |
| home-url | String | 任意页面路径 | '/pages/index/index' | 应用的首页地址。当从其它页面以reLaunch进入非首页时会自动显示首页按钮。 |
| title | String | - | 标题 | 导航栏标题文字 |
| black | Boolean | true/false | false | 暗黑模式 |
| showback | Boolean | true/false | true | 是否显示左边的返回和首页按钮 |
| flat | Boolean | true/false | false | 去除投影，边线 |
| transparent | Boolean | true/false | false | 开启透明顶部。下拉时会变成自定义的背景色。 |


</content-tm>


<content-tm>

### :point_right:Slot插槽属性表

| 属性名称 | 类型 | 数据 | 说明 |
| :-----: | :--: | :---: | :---: |
| default | Object | info | 默认插槽，中间位置 |
| left | Object | info | 插槽，左边位置 |
| right | Object | info | 插槽，右边位置 |

下面的结构体适合，自定插槽数据，设计个性化导航时，非常有作用。

**info结构**
```js{2}
{
	style:widths,//此处的结构见下方文档
	isTransparent:当前是否处于透明背景,
	title:导航标题
}

```
**info.style.widths结构**
```js
{
	btns: 右边小程序自带按钮组宽度,
}
```




</content-tm>


<content-tm>

### :point_right:更新日志

**2021年8月20日14:8:5**
初始版本。


</content-tm>

<testtime> </testtime>