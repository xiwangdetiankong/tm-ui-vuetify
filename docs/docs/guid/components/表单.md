---
title: tm-form 表单
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/comtwo/form"></mobile-tm>

<content-tm>

# tm-form 表单

:::tip 提醒

本组件<Badge text="1.1.7" />版本新增。

:::

:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:使用方法,跟数1,2,3一样简单
---

:::danger 特别说明

表单组件，本人不建议复杂的表单使用自动提交数据功能，建议自已手动提交到服务器。如果表单相对简单，则没问题。<br>
表单组件内部，一定需要放置一个提交按钮，且按钮上需要有表单专有的提交属性，并且该按钮的点按事件会被表单拦截，并触发
提交事件。请注意查看事例中的tm-button上的navtie-type属性为：form。<br>
已集成了对input校验功能，校验未通过，提交的事件会返回false,请详见文档。

:::

表单组件会收集以下组件的数据并形成以name为字段名的对象集合：
tm-input,tm-groupradio,tm-groupcheckbox,tm-upload,tm-rate,tm-slider,tm-stepper,tm-switch<br>
记住上面的组件放到form表单中时，需要提供name属性，如果不提供，则不收集。input会自动检验，如果是必填项的话。


<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<template>
	<view>
		<tm-form
		@submit="submit"
		ref="formData"
		@request="success"
		method="post">
			<tm-input name="title" required title="标题" v-model="reqData.title"></tm-input>
			<tm-input name="price" required title="价格" input-type="digit" v-model="reqData.price" suffix="万"></tm-input>
			<tm-button navtie-type="form" >提交数据</tm-button>
		</tm-form>
	</view>
</template>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
				reqData:{
					title:'',
					price:0
				}
			}
		},
		methods: {
			submit(e){
				//e为false表示校验未通过。否则为数据集合对象数据。
				if(e===false){
					uni.$tm.toast("请填写必填项。")
				}
			},
			//通过ref手动获取表单数据，不会验证。
			async getData(e){
				let d = await this.$refs.formData.getFormData();
				uni.showModal({
					title:"数据如下",
					content:JSON.stringify(d),
					showCancel:false
				})
			},
			//当前使用自动验证提交数据到服务器时，提交后，会触发此request事件，下面 的e是服务器返回的数据。
			success(e){
				//提交成功 。
				uni.$tm.toast(e.msg)
			}
		}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>



</content-tm>

<content-tm>

### :point_right:完整的demo示例
---

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<template>
	<view class="pb-50 text grey " :class="tmVuetify.black ? 'black bk' : ''">
		<tm-menubars title="表单示例" color="bg-gradient-blue-accent" :showback="true"></tm-menubars>
		<tm-form
		@submit="submit"
		ref="formData"
		@request="success"
		method="post"
		url="https://mockapi.eolinker.com/Uj8WNQX0d65502098df8ecc1f06348d7aac9364b4d08923/gongyingshang/fabu">
			<tm-sheet :shadow="24" :padding="[12,24]" :margin="[24,24]">
				<view class="py-12 px-24 mx-12 round-3 border-b-1 grey text" :class="tmVuetify.black ? 'bk' : ''">
					<text class="text-size-n text-weight-b ">车辆图片</text><text class="text-grey text-size-xs px-10">(最多可以上传8张)</text>
				</view>
				<view class="py-32 mx-12">
					<tm-upload
					 color="grey"
					 url="https://mockapi.eolinker.com/Uj8WNQX0d65502098df8ecc1f06348d7aac9364b4d08923/my/upload"
					 name="uploadImg" :filelist.sync="reqData.uploadImg" autoUpload :max="8" :grid="4"></tm-upload>
				</view>
				<view class="py-12 px-24 mx-12 round-3 border-b-1 grey text" :class="tmVuetify.black ? 'bk' : ''">
					<text class="text-size-n text-weight-b ">产品标题设置</text><text class="text-grey text-size-xs  px-10">(好的标题容易让人搜索到哦)</text>
				</view>
				<tm-input name="title" required title="标题" v-model="reqData.title"></tm-input>
				<tm-input name="price" required title="价格" input-type="digit" v-model="reqData.price" suffix="万"></tm-input>
				<tm-pickersCity ref="citypieek" :default-value.sync="reqData.city">
					<tm-input name="city" :border-bottom="false" required title="发布区域" placeholder="请选择城市区域" disabled :value="cityDisplay" right-icon="icon-angle-right"></tm-input>
				</tm-pickersCity>
				
				<view class="py-12 px-24 mx-12 round-3 border-b-1 grey text" :class="tmVuetify.black ? 'bk' : ''">
					<text class="text-size-n text-weight-b ">车辆信息</text><text class="text-grey text-size-xs  px-10">(请如实填写)</text>
				</view>
				<tm-pickers :default-value.sync="reqData.chexing" rang-key="title" :list="chelianglis">
					<tm-input name="chexing"  required title="车型" placeholder="请选择车型" disabled :value="obTstr2(reqData.chexing)" right-icon="icon-angle-right"></tm-input>
				</tm-pickers>
				<tm-pickersDate @confirm="chuchangchange" :default-value="reqData.chuchangtime">
					<tm-input name="chuchangtime" required title="出厂时间" placeholder="请选择出厂时间" disabled :value="reqData.chuchangtime" right-icon="icon-angle-right"></tm-input>
				</tm-pickersDate>
				<tm-input name="pailiang"  input-type="number"  title="车辆排量" placeholder="请输入"  v-model="reqData.pailiang" suffix="升(L)"></tm-input>
				<tm-input :vertical="true" required :height="150" input-type="textarea" bg-color="grey-lighten-5" :maxlength="200"  title="车辆描述" placeholder="请输入,不超过200字符"  v-model="reqData.beizu" ></tm-input>
				<view class="mx-32 my-12 border-b-1  pb-12 flex-between" :class="tmVuetify.black ? 'bk' : ''">
					<text class="text-size-n ">是否为事故车</text>
					<tm-groupradio name="shiguche" @change="shiguchechange">
						<tm-radio :name="item.title" v-for="(item,index) in shifoushiguche" :key="index" v-model="item.checked" :label="item.title"></tm-radio>
					</tm-groupradio>
				</view>
				<view class="mx-32 my-24 border-b-1  pb-24 flex-between" :class="tmVuetify.black ? 'bk' : ''">
					<text class="text-size-n ">发布后是否立即上架</text>
					<view>
						<tm-switch v-model="reqData.shangjia"></tm-switch>
					</view>
				</view>
				<view class="mx-32 my-24 border-b-1  pb-24 flex-between" :class="tmVuetify.black ? 'bk' : ''">
					<text class="text-size-n ">车辆评分</text>
					<view>
						<tm-rate name="pingfen" v-model="reqData.pingfen"></tm-rate>
					</view>
				</view>
				<view class="mx-32 my-24 border-b-1  pb-24 flex-between" :class="tmVuetify.black ? 'bk' : ''">
					<text class="text-size-n ">是否置顶车辆</text>
					<view>
						<tm-groupcheckbox name="zhiding">
							<tm-checkbox v-model="reqData.zhiding"></tm-checkbox>
						</tm-groupcheckbox>
					</view>
				</view>
				<view class="mx-32 my-24 border-b-1  pb-24 flex-between" :class="tmVuetify.black ? 'bk' : ''">
					<text class="text-size-n ">标签</text>
					<view>
						<tm-groupcheckbox name="bqm">
							<tm-checkbox :name="item.id" v-for="(item,index) in bam" :key="index" :label="item.title" v-model="item.checked"></tm-checkbox>
						</tm-groupcheckbox>
					</view>
				</view>
				<view class="mx-32 my-24 border-b-1  pb-24 flex-between" :class="tmVuetify.black ? 'bk' : ''">
					<text class="text-size-n ">估价(万)</text>
					<view style="width: 70%;">
						<tm-slider name="gujia" :show-left="true" :show-right="true" v-model="reqData.gujia"></tm-slider>
					</view>
				</view>
				<view class="mx-32 my-24 border-b-1  pb-24 flex-between" :class="tmVuetify.black ? 'bk' : ''">
					<text class="text-size-n ">有效时长</text>
					<view >
						<tm-stepper name="timeTep" :shadow="0" color="grey-lighten-4" v-model="reqData.timeTep"></tm-stepper>
						<text class="text-size-n pl-12">天</text>
					</view>
				</view>
				<view class="px-24">
					<tm-button navtie-type="form" theme="bg-gradient-blue-accent" block>提交数据</tm-button>
					<view class="py-32 text-size-s text-grey text-align-center">请注意资料的上传，必填项。</view>
				</view>
				
				<tm-button @click="getData" block theme="pink">手动获取表单数据,无验证</tm-button>
			</tm-sheet>
		</tm-form>
	</view>
</template>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
				chelianglis:[
					{title:"SUV",id:21},
					{title:"面包车",id:31},
					{title:"跑车",id:41},
					{title:"轿车",id:51},
					{title:"房车",id:61},
				],
				shifoushiguche:[
					{title:"是",checked:null},
					{title:"否",checked:null},
				],
				bam:[
					{title:"超大",id:1,checked:false},
					{title:"钻石车",id:3,checked:false},
					{title:"好车",id:4,checked:false},
				],
				reqData:{
					uploadImg:[],
					title:"",
					price:"",
					city:[{text:"上海市",id:31},{text:"市辖区",id:3101},{text:"普陀区",id:310107}],
					chexing:[{title:"跑车",id:41}],
					chuchangtime:"",
					pailiang:"",
					beizu:"",
					shiguche:[],
					shangjia:false,
					pingfen:0,
					gujia:0,
					zhiding:false,
					timeTep:0,
					bqm:[]
				}
			}
		},
		mounted() {
			this.$nextTick(function(){
				// console.log(this.$refs.citypieek.getSelectedValue());
			})
		},
		computed:{
			cityDisplay:function(){
				if(typeof this.reqData.city[0] ==='object'){
					let ds = [];
					this.reqData.city.forEach(item=>{
						ds.push(item.text)
					})
					return ds.join('/')
				}else if(typeof this.reqData.city[0] ==='string'){
					
					return this.reqData.city.join('/')
				}
			}
		},
		methods: {
			submit(e){
				if(e===false){
					uni.$tm.toast("请填写必填项。")
				}
			},
			async getData(e){
				let d = await this.$refs.formData.getFormData();
				uni.showModal({
					title:"数据如下",
					content:JSON.stringify(d),
					showCancel:false
				})
			},
			success(e){
				//提交成功 。
				uni.$tm.toast(e.msg)
			},
			obTstr(o){
				if(Array.isArray(o)){
					if(o.length.length===0) return "";
					return o.join(",")
				}
				return JSON.stringify(o)
			},
			obTstr2(o){
				if(Array.isArray(o)){
					if(o.length.length===0) return "";
					return o[0]?.title??""
				}
				return ''
			},
			chuchangchange(e){
				this.$set(this.reqData,'chuchangtime',e.year+"-"+e.month+"-"+e.day)
			},
			shiguchechange(e){
				if(e[0].index==0) {
					this.$set(this.reqData,"shiguche",true)
				}else{this.$set(this.reqData,"shiguche",false)}
			},
		}
	}
</script>
```
  </CodeGroupItem>
</CodeGroup>



</content-tm>


<content-tm>

### :point_right:props属性表

| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| url | String | - |  '' | 自动提交服务器时的网址 |
| method | String | POST/GET |  'POST' | 提交时的的方法 |
| header | Object | - |  {} | 提交时的头部对象数据 |


</content-tm>

<content-tm>

### :point_right:事件

| 事件名称 | 返回参数 | 返参类型 | 说明 |
| :-----: | :--: | :---: | :---: |
| @submit | 验证失败时返回false,通过时返回表单对象数据 | Boolean/Object | 点击表单中的button触发，不管验证通过与否都会触发 |
| @request | 服务器返回的数据 | - | 当填写了url时，提交表单会自动请求数据到服务器，成功后触发此事件 |

</content-tm>



<content-tm>

### :point_right:更新日志

**2021年9月16日晚**
初始版本。


</content-tm>

<testtime> </testtime>