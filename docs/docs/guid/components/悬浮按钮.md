---
title: tm-flotbutton 悬浮按钮
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/com/flotbutton"></mobile-tm>

<content-tm>

# tm-flotbutton 悬浮按钮
:::tip
这是一个非常强大的悬浮按钮，目前uniapp市场上的Ui框架，还未看到如此强大的悬浮按钮，漂亮且多达12个位置选择。随意控制位置和方向，以及自定义偏移量。
:::

:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:使用方法,跟数1,2,3一样简单
---

**基本示例**

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<tm-flotbutton label="添加" :show-text="true" color="bg-gradient-pink-accent" ></tm-flotbutton>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
				
			}
		},
		methods: {}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>



</content-tm>



<content-tm>

### :point_right:完整的示例
---

**可以复制到页面直接运行**
<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<template>
	<view class="">
		<tm-menubars title="轮播" color="bg-gradient-blue-accent" :transparent="true" :showback="true"></tm-menubars>

		<view>
			<tm-images src="https://picsum.photos/300?id=7"></tm-images>
			<tm-flotbutton :offset="[16,30]" actions-pos="left" position="topRight" label="上传" absolute :fixed="false" :show-text="true"
				color="bg-gradient-orange-accent" :actions="list"></tm-flotbutton>
			<view class="pa-32 pr-50 pt-50 blue-grey text">
				这是一个非常强大的悬浮按钮，总共多达12个位置选项共选择,具体请参考文档进行布局。总有一个位置你会满意。
			</view>
		</view>
		<tm-flotbutton label="添加" :show-text="true" color="bg-gradient-pink-accent" :actions="list"></tm-flotbutton>
		<tm-flotbutton position="bottomLeft" actions-pos="right" label="添加" :show-text="true"
			color="bg-gradient-blue-accent" :actions="list"></tm-flotbutton>
			<tm-images src="https://picsum.photos/300?id=52"></tm-images>
			<tm-images src="https://picsum.photos/300?id=23"></tm-images>
			<tm-images src="https://picsum.photos/300?id=29"></tm-images>
			<tm-images src="https://picsum.photos/300?id=288"></tm-images>
	</view>
</template>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
				list: [{
						icon: 'icon-commentdots-fill',
						color: 'bg-gradient-orange-accent'
					},
					{
						icon: 'icon-paperplane-fill',
						color: 'bg-gradient-green-accent'
					},
					{
						icon: 'icon-tag-fill',
						color: 'bg-gradient-blue-accent'
					}
				],
				offset: [16, 50]
			}
		},
		onLoad() {
			// #ifndef H5
			this.offset = [16, 99]
			// #endif
		},
		methods: {

		}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>

</content-tm>


<content-tm>

### :point_right:props属性表
:::warning 特别提醒
想要使用``` absolute ```相对父组件定位时，一定要把```fixed```设置为false <br>
即```:absolute="true" :fixed="false" ```<br>

---

可选属性不止下面这些，包含了所有原生可用的属性，具体参见[按钮](按钮.md)<br>
可以唤起，小程序客服，授权登录，获取手机号码等原生所有功能。

:::

| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| width | Number | 任意数字 | 0 | 自定义按钮大小 |
| font-size | Number | 22/23/24/26/28 | 22 | 文字大小 |
| icon-size | Number | 任意数字 | 36 | 图标大小 |
| size | String | xs/s/m/n/l/g | 'n' | 主按钮大小  |
| color | String | 任意主题色名称 | primary | 主按钮的主题颜色  |
| font-color | String | 任意主题色名称 | '' | 自定义子菜单文字颜色  |
| bgcolor | String | 任意颜色值16进制 | '' | 自定义子菜单背景颜色  |
| actions-pos | String | top / left / bottom / right / top | 子菜单按钮显示的方向  |
| icon | String | 任意图标名称 | '' | 默认主按钮图标  |
| label | String | - | '' | 主按钮下方的文字  |
| offset | Array | - |  [16, 16] | 自定义偏移量,单位upx  |
| position | String | topLeft / topRight / bottomRight / bottomLeft/top/bottom/left/right | bottomRight | 在absolute模式下没有left和right剧中。fixed模式包含所有模式。  |
| show-text | String | false/true | false | 是否显示文字。下下排列结构上图标，下文字。  |
| click-actions-hiden  | String | false/true | true | 点击子按钮菜单后是否隐藏所有子菜单。  |
| show-actions | String | false/true | false | 始终展开子菜单。点击子菜单后不消失.。  |
| absolute | String | false/true | false | 相对父组件定位,开启此定位，一定要把```fixed```设置为false  |
| fixed | String | false/true | true |  绝对定位，根据屏幕定位  |
| safe | String | false/true | false |  是否开启底部安全区域,当设置为bottom时（fixed模式下生效）会自动加上安全区域的高度  |
| actions | String | - | [] |  悬浮按钮展开的子按钮。格式[{icon:'图标名称',color:"颜色名称"}]  |



</content-tm>

<content-tm>

### :point_right:事件

| 事件名称 | 返回参数 | 返参类型 | 说明 |
| :-----: | :--: | :---: | :---: |
| @change | 返回顺序Index | Number | 子菜单按钮被点击触发事件。 |
| @click | - | - | 主按钮点击触发的事件 |

</content-tm>


<content-tm>

### :point_right:更新日志

**2021年8月20日14:8:5**
初始版本。


</content-tm>

<testtime> </testtime>