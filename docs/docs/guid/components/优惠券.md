---
title: tm-coupon 优惠券
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/comtwo/coupon"></mobile-tm>

<content-tm>

# tm-coupon 优惠券
:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:使用方法,跟数1,2,3一样简单
---

**基本示例**

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<view class="ma-32 "><tm-coupon color="deep-orange" :hdata="d_1"></tm-coupon></view>

```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
				d_1: {
					sale: '50',
					saleSplit: '￥',
					saleLable: '买满79元可用',
					title: '满79减15元券',
					time: '2021.11.11-2021.11.12 15:00:00',
					btnText: '去使用',
					label: '说明：优惠券说明优惠券说明优'
				},
			}
		},
		methods: {}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>



</content-tm>



<content-tm>

### :point_right:完整的示例
---

**可以复制到页面直接运行**
<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<template>
	<view>
		<tm-menubars title="表单示例" color="bg-gradient-blue-accent" :showback="true"></tm-menubars>
		<view class="ma-32 "><tm-coupon color="deep-orange" :hdata="d_1"></tm-coupon></view>
		<view class="ma-32"><tm-coupon color="deep-orange" :hdata="d_1" text></tm-coupon></view>
		<view class="ma-32"><tm-coupon color="deep-orange" :hdata="d_1" :disabled="true"></tm-coupon></view>
		<view class="flex-between ma-32">
			<view style="width: 320rpx;"><tm-coupon :hdata="d_2" color="bg-gradient-red-accent"></tm-coupon></view>
			<view style="width: 320rpx;"><tm-coupon :hdata="d_2" color="bg-gradient-deep-orange-accent"></tm-coupon></view>
		</view>
		<view class="ma-32" ><tm-coupon :hdata="d_3" text color="red"></tm-coupon></view>
	</view>
</template>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
import tmCoupon from '@/tm-vuetify/components/tm-coupon/tm-coupon.vue';
import tmMenubars from '@/tm-vuetify/components/tm-menubars/tm-menubars.vue';
export default {
	components: { tmCoupon ,tmMenubars},
	data() {
		return {
			d_1: {
				sale: '50',
				saleSplit: '￥',
				saleLable: '买满79元可用',
				title: '满79减15元券',
				time: '2021.11.11-2021.11.12 15:00:00',
				btnText: '去使用',
				label: '说明：优惠券说明优惠券说明优'
			},
			d_2: {
				sale: '90',
				saleSplit: '',
				saleLable: '积分获得',
				btnText: '领取券',
				suffix:'.6元'
			},
			
			d_3: {
				img:'https://picsum.photos/200',
				title: '满100减15元券',
				time: '2021.11.11-2021.11.12',
				btnText: '领取券',
				suffix: '.9元'
			}
		};
	}
};
</script>

```
  </CodeGroupItem>
</CodeGroup>

</content-tm>


<content-tm>

### :point_right:props属性表

| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| shadow | Number | 任意数字0-25 | 4 | 投影 |
| round | Number | 任意数字0-50 | 2 | 圆角 |
| color | String | 任意主题色名称 | primary | 主题色  |
| text | Boolean | true/false | false | 是否为文本模式，即减淡背景颜色。  |
| black | Boolean | true/false | false | 是否开启暗黑模式  |
| disabled | Boolean | true/false | false | 是否禁用（已使用）  |
| hdata | Object | 见下方数据格式 | {} | 当为数组时，自动变成轮播信息。  |

**hdata数据格式**
```js
	{
		img:'',//礼品图片，提供了图片，不显示数字和数字券说明。
		sale:'0',//优惠金额
		saleSplit:'￥',//前缀
		saleLable:'',//优惠金额正文说明文字
		title:'',//中间优惠券标题
		time:'',//有效时间
		btnText:'去使用',//按钮文本
		label:'',//底部优惠券额外说明
		suffix:''//优惠金额的后缀。比如：元
	}
```

</content-tm>

<content-tm>

### :point_right:事件

| 事件名称 | 返回参数 | 返参类型 | 说明 |
| :-----: | :--: | :---: | :---: |
| @click | 当前提供的hdata数据 | - | 点击按钮触发，如果被禁用不会触发 |

</content-tm>


<content-tm>

### :point_right:更新日志

**2021年8月20日14:8:5**
初始版本。


</content-tm>

<testtime> </testtime>