---
title: tm-message 提示框
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/comtwo/message"></mobile-tm>

<content-tm>

# tm-message 提示框
:tada:目录导航
[[toc]]

:::tip

相似的组件有[消息提示框](/guid/components/消息提示框.md)，[横幅消息](/guid/components/横幅消息提醒.md) ，和本组件提示框。<br>
本级应用场景为操作，加载中断操作时使用。
:::

</content-tm>

<content-tm>

### :point_right:使用方法,跟数1,2,3一样简单
---

**基本示例**

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<template>
	<tm-fullView>
		<template v-slot:default="{info}">
			<view class="grey text " :class="$tm.vx.state().tmVuetify.black?'black bk':''" :style="{minHeight:info.height+'px'}">
				<tm-menubars title="提示框" color="bg-gradient-primary-lighten"  :showback="true"></tm-menubars>
				
				<tm-message ref="toast"></tm-message>
				
				<view class="ma-32 white shadow-24 pa-24 round-6">
					<tm-button theme="white" @click="$refs.toast.show()" size='m'>info</tm-button>
					<tm-button  @click="$refs.toast.show({model:'load'})" size='m'>load</tm-button>
					<tm-button theme="red" @click="$refs.toast.show({model:'error'})" size='m'>error</tm-button>
					<tm-button @click="$refs.toast.show({model:'quest'})" size='m'>quest</tm-button>
					<tm-button theme="orange" @click="$refs.toast.show({model:'warn'})" size='m'>warn</tm-button>
					<tm-button theme="green" @click="$refs.toast.show({model:'success'})" size='m'>success</tm-button>
					<tm-button theme="pink" @click="$refs.toast.show({model:'disabled'})" size='m'>disabled</tm-button>
					<tm-button @click="$refs.toast.show({model:'wait'})" size='m'>wait</tm-button>
				</view>
				<view class="ma-32 white shadow-24 pa-24 round-6">
					
					<tm-button @click="$refs.toast.show({model:'load',mask:true})" size='m'>load无法穿透</tm-button>
					<tm-button @click="$refs.toast.show({model:'success',black:true,label:'我是修改的文字'})" size='m'>强制暗黑,并修改默认文字</tm-button>
					<tm-button @click="$refs.toast.show({model:'success',icon:'icon-wind-smile'})" size='m'>修改默认图标</tm-button>
					
				</view>
			</view>
		</template>
	</tm-fullView>

</template>

```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	import tmFullView from "@/tm-vuetify/components/tm-fullView/tm-fullView.vue"
	import tmMenubars from "@/tm-vuetify/components/tm-menubars/tm-menubars.vue"
	import tmSheet from "@/tm-vuetify/components/tm-sheet/tm-sheet.vue"

	import tmButton from "@/tm-vuetify/components/tm-button/tm-button.vue"
	import tmMessage from "@/tm-vuetify/components/tm-message/tm-message.vue"
	export default {
		components:{tmFullView,tmSheet,tmMenubars,tmButton,tmMessage
		},
		data() {
			return {
				
			}
		},
		mounted() {
			
		},
		methods: {
			
		}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>



</content-tm>


<content-tm>

### :point_right:props属性表

| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| color | String | 任意主题色名称 | {请见下方} | 主题色  |
| icon | String | 图标名称 | {请见下方} | 图标  |
| label | String | - | {请见下方} | 类型提示文字  |
| black | Boolean | - | false | 是否使用暗黑主题  |

**color,icon,label使用统一的格式**
```
 {
	load: 'primary',
	error: 'red',
	info: 'grey-darken-4',
	warn: 'orange',
	quest: 'primary',
	success: 'green',
	disabled: 'pink',
	wait: 'primary',
}

```
</content-tm>

<content-tm>

### :point_right:ref事件

| 事件名称 | 所需要参数 | 参数类型 | 说明 |
| :-----: | :--: | :---: | :---: |
| show | {label,model,icon,mask,wait,black} | Object | 显示提示框，参数可不填写 |
| hide | - | - | 隐藏提示框 |
```
//调用show所需要参数说明：
//调用show的参数可以不填写。
{
	label:提示文字,
	model:提示类型,load,error,info,warn,quest,success,disabled,wait
	icon:提示的图标,
	mask:是否显示遮罩，防止点击穿透,
	wait:显示多少秒ms后关闭,
	black:是否显示暗黑主题
}

//可以这样调用
$refs.ref.show()
//或者带参数
$refs.ref.show({model:'error'})

```


</content-tm>


<content-tm>

### :point_right:更新日志

**2021年8月20日14:8:5**
初始版本。


</content-tm>

<testtime> </testtime>