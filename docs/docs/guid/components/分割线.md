---
title: tm-divider 分割线
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/com/divider"></mobile-tm>

<content-tm>

# tm-divider 分割线
:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:使用方法,跟数1,2,3一样简单
---

**基本示例**

```vue
<tm-divider text="我是分割线"></tm-divider>
```
**不带文本**

```vue
<tm-divider  ></tm-divider>
```
**其它颜色**

```vue
<tm-divider color="red" model="dashed" text="红色分割线"></tm-divider>
```

**竖向**

```vue
<tm-divider :vertical="true" :width="1" model="dotted" text="竖向分割线"></tm-divider>

```

</content-tm>



<content-tm>

### :point_right:完整的示例
---

**可以复制到页面直接运行**
<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<template>
	<view>
		<tm-menubars title="divider" color="bg-gradient-blue-accent" :showback="true"></tm-menubars>
		<tm-sheet>
			<view class="pa-12 text-size-s text-weight-b blue text mb-32">基础示例</view>
			<tm-divider text="我是分割线"></tm-divider>
		</tm-sheet>
		<tm-sheet>
			<view class="pa-12 text-size-s text-weight-b blue text mb-32">线的种类</view>
			
			<tm-divider color="red" model="dotted" text="我是分割线"></tm-divider>
			<tm-divider color="red" model="dashed" text="我是分割线"></tm-divider>
			<view class="pa-12 text-size-s text-weight-b blue text mb-32">不带文本</view>
			<tm-divider  ></tm-divider>
		</tm-sheet>
		<tm-sheet >
			<view class="pa-12 text-size-s text-weight-b blue text mb-32">竖向</view>
			
			<view >
				<view class="d-inline-block">
					<tm-divider :vertical="true" :width="1" model="dotted" text="我是分割线"></tm-divider>
				</view>
				<view class="d-inline-block ml-32">
					<tm-divider :vertical="true" :width="1" :height="100" color="red" model="dotted" ></tm-divider>
				</view>
			</view>

		</tm-sheet>
	</view>
</template>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {

			}
		},
		methods: {
			
		}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>

</content-tm>


<content-tm>

### :point_right:props属性表
```js
* @property {String} text = [] 默认：'',显示的文本。
* @property {String} color = [] 默认：'grey',线的颜色
* @property {Number} height = [] 默认：0, 竖向高度时，起作用。
* @property {Boolean} vertical = [] 默认：false， 是否竖向
* @property {String} model = [solid|dashed|dotted] 默认：solid， 线的类型。

```

</content-tm>




<content-tm>

### :point_right:更新日志

**2021年8月20日14:8:5**
初始版本。


</content-tm>

<testtime> </testtime>