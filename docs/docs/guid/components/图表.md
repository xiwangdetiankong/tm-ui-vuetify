---
title: tm-echarts 图表
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/echarts/index"></mobile-tm>

<content-tm>

# tm-echarts 图表

:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:使用方法,跟数1,2,3一样简单
---

**基本示例**

:::tip 注意事项
本组件包超过500kb因此请注意分包大小。使用时，通过初始化能得到chart对象，可
操作相关函数（详见百度官方文档），但我不建议用此操作。我建议使用ref中的方法getChart().方法名来操作。
getChart()本身返回的是chart，支持链式调用。

---

同一个页面，可以渲染多个图表。需要在组件始化函数中手动调用ref来渲染图表，我不是不想直接在组件上增加数据属性来渲染那样的简便方法。实在是数据一多必卡。因此改用此方法渲染。
同时我不建议您将图表数据放入this对象或者组件对象上，非常影响性能。请通过ref函数操作。

:::

:::warning 关于同层渲染（图表无法被其它元素盖住）

echarts是采用canvas实现，在小程序和支付宝中，自动识别为2d渲染，其它平台appvue，h5是webgl本身支持同层。
<br>因此如果想要在同层渲染，被其它元素遮盖。那么
你的微信小程序的版本库（在开发者工具，右上角有个项目设置那）需要选择大于2.9.0（官方推荐）。
但为了兼容大部分手机，我建议选择2.12.3左右上下的版本。因为经过测试如果选择最新的2.19以上部分手机还是无法同层渲染。

:::

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue

<tm-echarts ref="teCart" @init="gc"></tm-echarts>

```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	<!-- 导入组件 -->
	import tmEcharts from '@/tm-vuetify/components/tm-echarts/tm-echarts.vue';
	export default {
		components: {tmEcharts},
		data() {
			return {
				
			}
		},
		methods: {
			gc(res) {
				// 初始化成功后返回的数据如下：
				const { width, height, chart } = res;
				//显示图表数据。
				this.$refs.teCart.setOption(this.rOpts('bar'));
			},
			rOpts(type) {
				return {
					color: ['#38caff', '#2099e9', '#668ce3'],
					tooltip: {
						trigger: 'axis',
						axisPointer: {
							// 坐标轴指示器，坐标轴触发有效
							type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
						},
						confine: true
					},
					legend: {
						data: ['热度', '正面', '负面']
					},
					grid: {
						left: 20,
						right: 20,
						bottom: 15,
						top: 40,
						containLabel: true
					},
					xAxis: [
						{
							type: 'value',
							axisLine: {
								lineStyle: {
									color: '#999'
								}
							},
							axisLabel: {
								color: '#666'
							}
						}
					],
					yAxis: [
						{
							type: 'category',
							axisTick: {
								show: false
							},
							data: ['汽车之家', '今日头条', '百度贴吧', '一点资讯', '微信', '微博', '知乎'],
							axisLine: {
								lineStyle: {
									color: '#999'
								}
							},
							axisLabel: {
								color: '#666'
							}
						}
					],
					series: [
						{
							name: '热度',
							type: type,
							label: {
								normal: {
									show: true,
									position: 'inside'
								}
							},
							data: [300, 270, 340, 344, 300, 320, 310],
							itemStyle: {
								emphasis: {
									color: '#37a2da'
								}
							}
						},
						{
							name: '正面',
							type: type,
							stack: '总量',
							label: {
								normal: {
									show: true
								}
							},
							data: [120, 102, 141, 174, 190, 250, 220],
							itemStyle: {
								emphasis: {
									color: '#32c5e9'
								}
							}
						},
						{
							name: '负面',
							type: type,
							stack: '总量',
							label: {
								normal: {
									show: true,
									position: 'left'
								}
							},
							data: [-20, -32, -21, -34, -90, -130, -110],
							itemStyle: {
								emphasis: {
									color: '#67e0e3'
								}
							}
						}
					]
				};
			},
		}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>


</content-tm>


<content-tm>

### :point_right:props属性表


| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| width | Number | 任意整数 | '100%' | 画布宽，默认使用父组件的宽高。单位rpx,可任意数字或者单位 |
| height | Number | 任意整数 | 500 | 画布高，单位rpx |
| canvasId | String | - | ec-canvas | 画布id，注意在MP小程序下更改无效，始终为默认值，H5情况下，此值同样失效，是一个随机的id。这是兼容（同一页面可放置多疑图表）的问题，无法更改，虽然提供此值，但不能修改。 |

</content-tm>

<content-tm>

### :point_right:事件

| 事件名称 | 返回参数 | 返参类型 | 说明 |
| :-----: | :--: | :---: | :---: |
| @init | {width,height,chart} | Object | 图表初始化成功后执行 |
| @error | - | Object | 出错时触发 |

</content-tm>

<content-tm>

### :point_right:ref方法
需要通过```$refs.ref```来实现。
| 方法名称 | 所需参数 | 返参 | 说明 |
| :-----: | :--: | :---: | :---: |
| setOption | JSON见百度文档 | - | 配置图表数据，初始化成功后调用此显示图表 |
| getChart | - | chart | 初始化成功后调用返回图表对象，方法见百度官方文档 |

</content-tm>



<content-tm>

### :point_right:更新日志

**2021年8月20日14:8:5**
初始版本。


</content-tm>

<testtime> </testtime>