---
title: tm-pagination 分页
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/com/pagination"></mobile-tm>

<content-tm>

# tm-pagination 分页
:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:基本示例
---

<CodeGroup>
  <CodeGroupItem title="vue">

```vue{2,4}
<tm-pagination  color="bg-gradient-blue-accent" 
:page.sync="ym"  :total="100" 
:totalVisible="5"></tm-pagination>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" active>

```js{5}
<script>
	export default {
		data() {
			return {
				ym:1
			}
		}
	}
</script>

```

  </CodeGroupItem>
</CodeGroup>

</content-tm>

<content-tm>

### :point_right:更换主题
属性```color="颜色名称"```即可。
```vue{3,6,9}

<tm-pagination  
color="green" 
:page.sync="ym"  :total="100" :totalVisible="5"></tm-pagination>
<tm-pagination  
color="red" 
:page.sync="ym"  :total="100" :totalVisible="5"></tm-pagination>

```

</content-tm>

<content-tm>

### :point_right:暗黑
组件加上```black``` props属性即可.

```vue{4}

<tm-sheet black>
	<view class="text-size-s text-weight-b">暗黑</view>
	<tm-pagination black color="bg-gradient-blue-accent" :page.sync="ym"  :total="100" :totalVisible="5"></tm-pagination>
	
</tm-sheet>

```

</content-tm>

<content-tm>

### :point_right:外观,圆形和方形
组件加上```round``` props属性即可.

```vue{2}
<tm-pagination 
round 
color="bg-gradient-blue-accent" :page.sync="ym"  
:total="100" 
:totalVisible="5">
</tm-pagination>

```

</content-tm>



<content-tm>

### :point_right:props属性表


| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| page | Number | 任意整数 | 1 | 当前页码，必须使用page.sync |
| total-visible | Number | 任意整数 | 5 | 最大可见页数，不得小于5 |
| total | Number | 任意整数 | 0 | 总条数 |
| size | Number | 任意整数 | 10 | 每页的条目数量 |
| round | Boolean | true\|false | false | 是否圆形按钮 |
| black | Boolean | true\|false | false | 是否暗黑模式 |
| disabled | Boolean | true\|false | false | 禁用 |
| color | String | 任意主题色名 | primary | 选中的主题色 |

</content-tm>



<content-tm>



### :point_right:事件

| 事件名称 | 返回参数 | 返参类型 | 说明 |
| :-----: | :--: | :---: | :---: |
| @change | 当前页码 | number | 页码改变时触发 |


示例

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue{2}
<tm-pagination  
@change="change"
color="bg-gradient-blue-accent" :page.sync="ym"  
:total="100" :totalVisible="5"></tm-pagination>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js{5,11}
<script>
	export default {
		data() {
			return {
				ym:1
			}
		}
	},
	methods: {
		change(e){
			console.log(e);
		}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>

</content-tm>



<content-tm>

### :point_right:更新日志

**2021年8月20日14:8:5**
初始版本。


</content-tm>

<testtime> </testtime>