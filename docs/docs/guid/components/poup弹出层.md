---
title: tm-poup poup弹出层
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/com/poup"></mobile-tm>

<content-tm>

# tm-poup poup弹出层
:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:使用方法,跟数1,2,3一样简单
---

**基本示例**

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<template>
	<view>
		<tm-menubars title="poup弹出层" color="bg-gradient-blue-accent" :showback="true"></tm-menubars>
		<tm-sheet color="blue text ">
			<tm-button @click="pos='bottom'; show_1=true">底部弹出</tm-button>
			<tm-button @click="pos='top'; show_1=true">顶部弹出</tm-button>
			<tm-button @click="pos='left'; show_1=true">左边弹出</tm-button>
			<tm-button @click="pos='right'; show_1=true">右边弹出</tm-button>
			<tm-button @click="pos='center'; show_1=true">中间弹出</tm-button>
			<tm-poup v-model="show_1" :position="pos"></tm-poup>
		</tm-sheet>
	</view>
</template>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
				show_1:false,
				pos:'bottom'
			}
		},
		methods: {
			
		}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>



</content-tm>

<content-tm>

### :point_right:props属性表

| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| width | Number|String | 任意数字 | 0 | 位置为left,right是起作用。可以是30%或者数字(单位upx) |
| height | Number|String | 任意数字 | 0 | 位置为top,bottom是起作用。可以是30%或者数字(单位upx) |
| round | Number | 圆角0-25 | 0 | 圆角 |
| bg-color | String | 任意主题色名称 | white | 请填写背景的主题色名称  |
| over-color | String | 颜色值，不是主题名称 | rgba(0,0,0,0.3) | 遮罩层颜色值  |
| clss-style | String | - | '' | 自定内容的类  |
| position | String | bottom,left,right,top,center | '' | 弹层方向方向  |
| value | Boolean | true/false | false | 使用时value.sync可同步，也可不同步。等同于v-model |
| over-close | Boolean | true/false | false |  是否允许点击遮罩关闭 |
| black | Boolean | true/false | false |  暗黑模式 |
| is-filter | Boolean | true/false | true |  是否背景模糊 |


</content-tm>

<content-tm>

### :point_right:事件

| 事件名称 | 返回参数 | 返参类型 | 说明 |
| :-----: | :--: | :---: | :---: |
| @change | 等同于v-model和value | Boolean | 显示和隐藏切换时触发 |

</content-tm>


<content-tm>

### :point_right:更新日志

**2021年8月20日14:8:5**
初始版本。


</content-tm>

<testtime> </testtime>