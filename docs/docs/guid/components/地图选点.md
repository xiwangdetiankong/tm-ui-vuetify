---
title: tm-mapSelectedPoint 地图选点
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/comtwo/mapSelectedPoint"></mobile-tm>

<content-tm>

# tm-mapSelectedPoint 地图选点

:::warning 提醒
地图定位，需要你按照官方文档配置地图密钥。不要问我怎么配置[请前往官方文档查看](https://uniapp.dcloud.net.cn/component/map)<br>
当前只支持腾讯地图，后续版本，支持百度，高德地图。<br>
<span style="color:red">为什么我复制到小程序运行定位失败？</span><br>
1、检查密钥。2、检查是否配置，勾选了地图在mainfest.json中。3、是否把腾讯地图域名加入白名单：https://apis.map.qq.com
4、测试时，只能在内置浏览器或者开发工具中获取。5、真机调试未设置域名单，需要打开调试，触发测试限制。6、其它注意事项看官方文档。

---

正式版本发布，需要把上面配置好。并且是在https下。

----
[uniapp官方说明](https://uniapp.dcloud.net.cn/component/map)

:::

:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:使用方法,跟数1,2,3一样简单
---

**基本示例**

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<tm-mapSelectedPoint width="686" height="1000"></tm-mapSelectedPoint>

```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
			}
		},
		methods: {}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>


</content-tm>



<content-tm>

### :point_right:完整的示例
---

**可以复制到页面直接运行**
<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<template>
	<tm-fullView>
		<template v-slot:default="{ info }">
			<view class="grey text " :class="[$tm.vx.state().tmVuetify.black ? 'black bk' : '']" :style="{ minHeight: info.height + 'px' }">
				<tm-menubars title="地图选点" color="bg-gradient-primary-lighten" :showback="true"></tm-menubars>
				<view class="ma-32 flex-between flex-wrap round-10 overflow white shadow-24">
					<tm-mapSelectedPoint :location="mp" width="686" height="1000"></tm-mapSelectedPoint>
				</view>
				<view class="ma-32"><tm-button :round="24" @click="mp={latitude:28.68268,longitude:115.85817}" block>赋值组件反选已有定位点</tm-button></view>

			</view>
		</template>
	</tm-fullView>
</template>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
import tmMapSelectedPoint from '@/tm-vuetify/components/tm-mapSelectedPoint/tm-mapSelectedPoint.vue';
import tmMenubars from '@/tm-vuetify/components/tm-menubars/tm-menubars.vue';
import tmFullView from '@/tm-vuetify/components/tm-fullView/tm-fullView.vue';
import tmButton from '@/tm-vuetify/components/tm-button/tm-button.vue';
export default {
	components: { tmMapSelectedPoint, tmMenubars,tmFullView,tmButton },
	data() {
		return {
			mp:{latitude:0,longitude:0}
		};
	}
};
</script>

```
  </CodeGroupItem>
</CodeGroup>

</content-tm>


<content-tm>

### :point_right:props属性表

| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| black | Boolean | true/false | null | 是否开启暗黑模式 |
| color | String | 任意主题名称 | grey-lighten-3 | 背景色。 |
| scale | Number | 5-18 | 14 | 地图缩放级别 |
| width | Number | 任意整数 | 750 | 组件的宽度rpx |
| height | Number | 任意整数 | 1200 | 组件的高度rpx |
| location | Object | - | {latitude:0,longitude:0} | 自动定位当前位置，如果提供，定位到此点。 |
| map-type | String | qq,baidu,amp高德 | qq | 地图类型，当前不可更改，后续版本开放。 |
| map-key | String | - | 作者已提供测试key | 请前往对应地图提供商申请key，勾选webserve服务 |


</content-tm>

<content-tm>

### :point_right:事件

| 事件名称 | 返回参数 | 返参类型 | 说明 |
| :-----: | :--: | :---: | :---: |
| @confirm | 见下方 | Objec | 当地图控件中，点击确认位置是触发 |
| @change | 见下方 | Objec | 当地图移动时触发 |

**上方事件返回的参数类型如下**
```
{
	adress:'当前定位的地址',
	city:[省，市，县/区]
}

```

</content-tm>

<content-tm>

### :point_right:更新日志

**2021年8月20日14:8:5**
初始版本。


</content-tm>

<testtime> </testtime>