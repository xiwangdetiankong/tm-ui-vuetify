---
title: tm-button 按钮
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/com/button"></mobile-tm>

<content-tm>

# tm-button 按钮

:::warning 特别提醒
按钮的众多属性与原生按钮是一致的。
:::


:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:使用方法,跟数1,2,3一样简单
---
:::tip 技巧
按钮划分为：xs,s,m,n,l,g尺寸。block为100%宽度。<br>
按钮背景模式划分为:plan镂空，text浅背景，titl无背景
:::

```vue
<tm-button theme="light-green">标准</tm-button>
```

</content-tm>



<content-tm>

### :point_right:完整的示例
---

**可以复制到页面直接运行**
<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<template>
	<view>
		<tm-menubars title="按钮" color="white" theme="white" :showback="true"></tm-menubars>
		<tm-sheet >
			<view class="text-size-s text-weight-b ">
				基本示例
			</view>
			<tm-button theme="green" size="xs">特小</tm-button>
			<tm-button theme="indigo" size="s">小</tm-button>
			<tm-button theme="cyan" size="m">中</tm-button>
			<tm-button theme="light-green" size="n">标准</tm-button>
			<tm-button theme="bg-gradient-green-accent" size="l">较大</tm-button>
			<tm-button theme="bg-gradient-red-accent" size="g">大</tm-button>
			<tm-button  theme="bg-gradient-orange-accent" :round="24" block>block</tm-button>
			<view class="py-12"></view>
			<tm-button :loading="true" theme="bg-gradient-pink-accent" :round="24" black block>loading</tm-button>
			<view class="py-12"></view>
			<tm-button plan theme="pink"  >block</tm-button>
			<tm-button text theme="blue"  >block</tm-button>
			<tm-button text theme="white"  >block</tm-button>
		</tm-sheet>
		<tm-sheet >
			<view class="text-size-s text-weight-b ">
				fab
			</view>
			<tm-button fab icon="icon-pengyouquan" theme="light-green" size="xs">n</tm-button>
			<tm-button fab icon="icon-QQ" icon-size="30" theme="bg-gradient-orange-accent" size="s">l</tm-button>
			<tm-button fab icon="icon-weixin" icon-size="45" theme="bg-gradient-indigo-accent" size="m">g</tm-button>
			<tm-button :loading="true" fab icon="icon-pengyouquan" icon-size="60" theme="bg-gradient-green-accent" size="n">n</tm-button>
			<tm-button fab icon="icon-QQ" icon-size="70" theme="bg-gradient-blue-accent" size="l">l</tm-button>
			<tm-button titl fab fontColor="red" icon="icon-weixin" icon-size="90"  size="g">g</tm-button>
		</tm-sheet>
	</view>
</template>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
			}
		},
		methods: {

		}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>

</content-tm>


<content-tm>

### :point_right:props属性表

:::danger 注意
**官方文档介绍[点此了解](https://uniapp.dcloud.io/component/button)**
 <br>注意，按钮功能同原生按钮功能，所有属性都相同。
:::

| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| font-size | Number | 任意数字 | 0 | 自定义文字大小，默认使用主题size的大小 |
| shadow | Number | 0-25 | 5 | 按钮投影 |
| width | Number | 任意数字 | 0 | 按钮宽,  单位px ,可以是百分比 如果不设置block为true时，此项设置无效。 |
| height | Number | 任意数字 | 0 | 按钮高, 单位px ,可以是百分比如果不设置block为true时，此项设置无效。 |
| round | Number | 0-25 | 0 | 圆角 |
| icon-size | Number | 任意数字 | 0 | 自定义图标大小，单位upx，默认使用主题size的大小 |
| bgcolor | String | 颜色值，非主色名称 | '' | 自定义-背景颜色  |
| theme | String | 任意主题色名称 | 'primary' | 主题背景颜色  |
| font-color | String | 任意主题色名称 | 'white' | 文字和图标颜色  |
| icon | String | 图标名称或者图片地址 | '' | 自定义按钮图标  |
| item-class | String | 任意类名 | '' | 按钮自定样式类  |
| size | String | xs/s/m/n/l/g | 'n' | 按钮尺寸大小  |
| url | String | 页面地址 | '' | 如果提供将会跳转连接  |
| navtie-type<Badge text="1.1.7" /> | String | form | '' | 可选值form，提供此属性为form时，事件会被表单接替，会触发表单提交事件。  |
| open-type | String | contact/getPhoneNumber<br>getUserInfo/launchapp<br>share/openSetting | '' | 同原生btn相同  |
| label | String | - | '' | 按钮文字也可以使用插槽模式直接写在组件内部。  |
| disabled | Boolean | true/false | false | 是否禁用 |
| loading | Boolean | true/false | false | 是否加载中 |
| block | Boolean | true/false | false | 是否独占一行,宽度100% |
| show-value | Boolean | true/false | false | fab模式是隐藏文字的。如果这个设置为true,不管在什么情况下都会显示文字。 |
| black | Boolean | true/false | false | 暗黑模式 |
| flat | Boolean | true/false | false | 去除所有投影圆角效果，扁平模式。 |
| text | Boolean | true/false | false | 文本模式。背景减淡。文字使用主题色。 |
| plan | Boolean | true/false | false | 镂空 |
| fab | Boolean | true/false | false | 图标圆模式 |
| dense | Boolean | true/false | false | 是否去除自带的四周边距 |
| titl | Boolean | true/false | false | 无背景，无边框，只保留按钮区域和文字颜色或者图标颜色。 |
| userProfileError | String | auto/任意字符 | 'auto' | 仅open-type='getUserProfile'时有效，当微信授权失败提示的信息，如果非auto将显示自定义错误提示。如果为''，将不显示错误提示。  |

</content-tm>

<content-tm>

### :point_right:关于微信用户授权

:::danger 注意
这是专门为微信授权定制的专有属性。
:::

当需要使用微信授权登录时，请在把属性open-type="getUserInfo"或者"getUserProfile"<br>
然后按钮将会触发事件getUserinfo或者getUserProfile。返回的信息为用户信息。<br>
示例：
```
<tm-button  openType="getUserInfo" @getUserInfo="getUser">微信授权登录</tm-button>
//请注意，当用户拒绝授权时，将不会触发getUserInfo和getUserProfile事件。也就收不到下方的事件信息了。
//js中
getUser(e){
	//输出e,
	//返回的 e 即为微信的个人用户信息。
}

```


</content-tm>

<content-tm>

### :point_right:事件

**官方文档介绍[点此了解](https://uniapp.dcloud.io/component/button)**

| 事件名称 | 返回参数 | 返参类型 | 说明 |
| :-----: | :--: | :---: | :---: |
| @contact | 同原生 | - | - |
| @error | 同原生 | - | - |
| @getphonenumber | 同原生 | - | - |
| @getUserinfo | 同原生 | - | - |
| @opensetting | 同原生 | - | - |
| @click | 同原生 | - | - |



</content-tm>

<content-tm>

### :point_right:按钮组示例
---


<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<template>
	<view>
		<tm-menubars title="按钮" color="white" theme="white" :showback="true"></tm-menubars>
		<tm-sheet :shadow="24">
			<view class="text-size-s text-weight-b ">
				按钮组
			</view>
			
			<tm-groupButton :round="2">
				<tm-button theme="blue" size="s" icon="icon-plus"></tm-button>
				<tm-button theme="blue" size="s" icon="icon-minus"></tm-button>
				<tm-button theme="blue" size="s" icon="icon-times"></tm-button>
				<tm-button theme="blue" size="s" icon="icon-delete"></tm-button>
			</tm-groupButton>
			<tm-groupButton>
				<tm-button theme="red" size="m">删除</tm-button>
				<tm-button theme="blue" size="m">新建</tm-button>
				<tm-button theme="green" size="m">添加</tm-button>
			</tm-groupButton>
		</tm-sheet>
	</view>
</template>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
			}
		},
		methods: {

		}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>

</content-tm>

<content-tm>

### :point_right:props按钮组属性表

:::danger 注意

注意，按钮组中，只允许放置按钮组件。
:::

| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| round | Number | 0-25 | 0 | 圆角 |
| margin | Array(Number) | 0-50 | [0,24] | x,y的间距 |
| active-color | String | 任意主题色 | primary | 点按激活的颜色主题 |


</content-tm>

<content-tm>

### :point_right:props按钮组事件

:::danger 注意

注意，按钮组中，只允许放置按钮组件。
:::

| 事件名称 | 返回参数 | 参数类型 | 是否需要参数 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| change | 当前按钮索引 | number | - | 在按钮组点点击按钮时触发 |


</content-tm>

<content-tm>

### :point_right:更新日志

**2021年8月20日14:8:5**
初始版本。


</content-tm>

<testtime> </testtime>