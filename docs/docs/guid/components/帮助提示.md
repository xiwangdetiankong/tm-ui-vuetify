---
title: tm-helpTips 弹出菜单
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/com/helpTips"></mobile-tm>

<content-tm>

# tm-helpTips 弹出菜单

:::tip 提醒

本组件<Badge text="1.1.1" />版本新增。主要应用于，文档说明，解释说明的问号帮助。

:::

:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:使用方法,跟数1,2,3一样简单
---

**基本示例**

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<tm-helpTips ment-direction="right" tip-direction="right"  label="每点击一次增加一个积分哦,你知道了吗？">
	<tm-icons name="icon-question-circle"></tm-icons>
</tm-helpTips>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
			}
		},
		methods: {}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>



</content-tm>



<content-tm>

### :point_right:完整的示例
---

**可以复制到页面直接运行**
<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue
<template>
	<view>
		<tm-menubars title="帮助提示" color="bg-gradient-blue-accent"  :showback="true"></tm-menubars>
		<tm-sheet :shadow="24">
			什么是积分？点击右边问号图标展示
			<tm-helpTips ment-direction="right" tip-direction="right"  label="每点击一次增加一个积分哦,你知道了吗？">
				<tm-icons name="icon-question-circle"></tm-icons>
			</tm-helpTips>
		</tm-sheet>
		<tm-sheet :shadow="24">
			<view class="text-align-center">
				什么是积分？<tm-helpTips color="blue" :show="true" direction="top"  label="每点击一次增加一个积分哦,你知道了吗？">
				<tm-icons name="icon-question-circle"></tm-icons>
			</tm-helpTips>点击问号图标展示
			</view>
			
		</tm-sheet>
	</view>
</template>
```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	export default {
		data() {
			return {
			}
		},
		methods: {
			
		}
	}
</script>

```
  </CodeGroupItem>
</CodeGroup>

</content-tm>


<content-tm>

### :point_right:props属性表

| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| tip-direction | String | left/center/right | center | 指示三角形的方向，小三角也可以，左中右对齐  |
| direction | String | top/bottom |  bottom | 内容框竖向的弹出方向，向上和向下弹出 |
| ment-direction | String | left/center/right |  center | 内容框水平方向的对齐方式，左中右对齐 |
| rang-key | String | - |  title | 菜单列表数组对象时，需要提供key |
| icon-color | String | 任意主题色名称 |  white | 关闭图标颜色主题名称 |
| color | String | 任意主题色名称 |  primary | 主题背景色 |
| black | Boolean | true/false | false | 暗黑模式 |
| show | Boolean | true/false | false | 显示菜单，用于默认显示的功能，如需双向绑定使用show.sync,也可不提供  |
| label | String | - | [] | 需要显示的内容 |



</content-tm>






<content-tm>

### :point_right:更新日志

**2021年8月20日14:8:5**
初始版本。


</content-tm>

<testtime> </testtime>