---
title: tm-cartCellListFood 零售商品购物车列表
---
<mobile-tm src="https://jx2d.cn/uniapp/#/subpages/food/cartCellListFood"></mobile-tm>

<content-tm>

# tm-cartCellListFood 零售商品购物车列表
:tada:目录导航
[[toc]]

</content-tm>

<content-tm>

### :point_right:基本示例
---

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue

<tm-cartCellListFood border="" :mdata="test" :cart-num.sync="test.buy"></tm-cartCellListFood>

```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
export default {
	data() {
		return {
			test: {
				img: 'https://picsum.photos/300?k=1',
				title: '产品3（任选）',
				label: '这个产品是只有几个融会',
				price: 36.2,
				salePrice: 76.4,
				saleLabel: ['7折优惠', '首单减3元'],
				unit: '/斤',
				id: 3,
				buy: 0,
				itemId: 0
			},
		};
	},
	mounted() {
		
	},
	methods: {}
};
</script>

```

  </CodeGroupItem>
</CodeGroup>

</content-tm>




<content-tm>

### :point_right:完整的示例

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue

<template>
	<view class="grey text " :class="tmVuetify.black ? 'black bk' : ''" :style="{ height: bodyHeight + 'px' }">
		<tm-menubars title="购物车商品操作列表" color="bg-gradient-blue-accent" :showback="true"></tm-menubars>
		<tm-sheet :padding="[0, 0]">
			<tm-cartCellListFood border="" :mdata="test" :cart-num.sync="test.buy"></tm-cartCellListFood>
			<tm-cartCellListFood border="" :mdata="test" :cart-num.sync="test.buy"></tm-cartCellListFood>
			<tm-cartCellListFood border="" :black="true" :mdata="test" :cart-num.sync="test.buy"></tm-cartCellListFood>
			<tm-cartCellListFood border="" :black="true" :mdata="test" :cart-num.sync="test.buy"></tm-cartCellListFood>
		</tm-sheet>
	</view>
</template>

```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
export default {
	data() {
		return {
			test: {
				img: 'https://picsum.photos/300?k=1',
				title: '产品3（任选）',
				label: '这个产品是只有几个融会',
				price: 36.2,
				salePrice: 76.4,
				saleLabel: ['7折优惠', '首单减3元'],
				unit: '/斤',
				id: 3,
				buy: 0,
				itemId: 0
			},
			bodyHeight: 50
		};
	},
	mounted() {
		
		let sy = uni.getSystemInfoSync();
		this.bodyHeight = sy.windowHeight;
	},
	methods: {}
};
</script>

```

  </CodeGroupItem>
</CodeGroup>



</content-tm>


<content-tm>

### :point_right:props属性表

| 属性名称 | 类型 | 可选值 | 默认值 | 说明 |
| :-----: | :--: | :---: | :---: | :--- |
| black | Boolean | true\|false | null | 是否暗黑模式 |
| color | String | 任意主题色名 | 'primary' | 主题色 |
| bg-color | String | 任意主题色名 | 'white' | 背景主题色 |
| img-width | Number | 任意整数 | 140 | 图片元素宽度，rpx单位 |
| cart-num | Number | 任意整数 | 0 | 当前销售的数量，需要cartNum.sync |
| action-size | Number | 任意整数 | 34 | 增减按钮大小。单位rpx |
| dense | Boolean | true\|false | false | 是否隐藏中间优惠和文字说明的结构，只保留标题和价格数量按钮。 |
| border | String | top/bottom | top | 显示上边线还是下边线 |
| mdata | String | - | {} | 数据结构 |
| key-map | String | - | {} | mdata的字段映射，每个人数据格式都不一样，如果你的数据和默认字段不一致，需要映射下关键字段。 |

因每个人的数据结构字段不一样，因此```key-map```就是映射mdata字段名，以此来各个不一样的数据结构体

**mdata结构**
```js
// [必要]是该属性一定要存在,不然报错.
{
	img:'https://picsum.photos/300?k=2', //产品图片 [必要]
	title:'特色单人套餐（任选）', //产品标题 [必要]
	label:'这个产品是只有几个融会', //产品介绍
	price:36.2, //价格 [必要]
	salePrice:76.4, //原价
	saleLabel:['7折优惠','首单减3元'],//宣传营销的字词组,没有为[]即可
	unit:'/斤' //价格的单位,没有可为空 ''
	buy:0 //当前销售的数量  [必要]
}
//如果你的数据结构体与上方名称不致,可通过下方key-map属性映射你自己的字段.

```
**key-map默认映射的字段就是默认mdata的基础结构**
```
{
	img:'img',
	title:'title',
	label:'label',
	price:'price',
	salePrice:'salePrice',
	saleLabel:'saleLabel',
	unit:'unit'
	buy:'buy'
}

```

</content-tm>



<content-tm>



### :point_right:事件

| 事件名称 | 返回参数 | 返参类型 | 说明 |
| :-----: | :--: | :---: | :---: |
| @change | 数量 | number | 改变销售数量时触发，返回当前改变后的销售数量。 |



</content-tm>



<content-tm>

### :point_right:更新日志

**2021年9月12日**
初始版本。


</content-tm>


<testtime> </testtime>