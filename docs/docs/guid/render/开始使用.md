---
title: render开始使用
---

<content-tm>


### :leopard:开始使用

因为tm-render使用非常的简单，太容易上手了。<br>
使用前需要引入相关组件：

```js
//在js中引入组件
import tmRender from '@/tm-vuetify/components/tm-render/tm-render.vue';
```

```
//在vue中键入组件
<tm-render ></tm-render>

```
这样你就完成最基本的前期引入操作和使用操作。<br>
接下来你需要了角组件的属性和方法，请前往[查看组件方法及属性](/guid/render/组件属性及方法.md)<br>

下面是创建一个基础的矩形框：

```vue

<template>
	<view>
		<tm-fullView>
			<template v-slot:default="{ info }">
				<view class="grey text " :class="$tm.vx.state().tmVuetify.black ? 'grey-darken-6 bk' : ''"
					:style="{ minHeight: info.height + 'px' }">
					<tm-render :height="950" ref="render" @render="ok"></tm-render>
				</view>
			</template>
		</tm-fullView>
	</view>
</template>

```

```js

<script>
	import tmRender from '@/tm-vuetify/components/tm-render/tm-render.vue';
	export default {
		components: {
			tmRender,
		},
		data() {
			return {
			};
		},
		methods: {
			//render创建成功的回调。
			ok(e) {
				// 渲染完成。
				const [w, h] = e;
				let t = this;
				t.$refs.render.addGraph({
					name: 'rect',//需要创建的图形名称
					animationCurve: "easeOutBack",//动画效果
					drag:true,//表示此图形允许拖动
					shape: {
						x: 0,//图形的x坐标
						y: 0,//图形的y坐标
						w: w,//图形的宽度
						h: h //图形的高度
					},
					style: {
						fill: 'rgba(0,98,255,1)' //需要填充的颜色
					}
				})
			}
		}
	};
</script>

```

</content-tm>

<testtime> </testtime>