---
title: tm-render render示例介绍
---
<mobile-tm src="https://jx2d.cn/uniapp/#/pages/index/crender"></mobile-tm>



<content-tm>

### :point_right:使用方法,跟数1,2,3一样简单
---

**示例**

<CodeGroup>
  <CodeGroupItem title="vue" active>

```vue

<template>
	<view>
		<tm-fullView>
			<template v-slot:default="{ info }">
				<view class="grey text " :class="$tm.vx.state().tmVuetify.black ? 'grey-darken-6 bk' : ''"
					:style="{ minHeight: info.height + 'px' }">
					<tm-menubars title="反馈bug或者意见"></tm-menubars>

					<view class="exid">

						<tm-render :height="950" ref="render" @render="ok"></tm-render>
						<view class="flex-center ma-32">
							<tm-button @click="crateAni">重播tm动画</tm-button>
							<tm-button @click="createRingAni">查看示例动画</tm-button>
						</view>
						<view class="ma-32 pa-24 shadow-24 round-10 text-size-n">
							canvas渲染引擎,多图层渲染，单独控制属性动画内置拖动。内置常用的图形元素。
							请注意还未完全兼容appvue。H5和微信小程序已完全兼容。
						</view>
					</view>

				</view>
			</template>
		</tm-fullView>
	</view>
</template>

```
  </CodeGroupItem>

  <CodeGroupItem title="JS" >

```js
<script>
	import tmRender from '@/tm-vuetify/components/tm-render/tm-render.vue';
	import tmMenubars from '@/tm-vuetify/components/tm-menubars/tm-menubars.vue';
	import tmButton from '@/tm-vuetify/components/tm-button/tm-button.vue';
	import tmFullView from '@/tm-vuetify/components/tm-fullView/tm-fullView.vue';
	import ring from '@/tm-vuetify/tool/function/crender/graph/ring';
	import path2d from '@/tm-vuetify/tool/function/crender/graph/path2d.js';

	let graph = null;
	export default {
		components: {
			tmRender,
			tmButton,
			tmFullView,
			tmMenubars
		},
		data() {
			return {
				isCanvasOk: false,
				stop: false
			};
		},
		mounted() {},

		methods: {
			ok(e) {
				// 渲染完成。
				console.log('渲染完成');
				const [w, h] = e;
				let t = this;
				let rectWidth = 100;
				let rectHeight = 60;
				this.isCanvasOk = true;
				this.crateAni();
				
			},
			async crateAni() {
				if (!this.isCanvasOk) return;
				this.stop = true;

				let t = this;
				let crender = this.$refs.render.getRender();
				crender.graphs.forEach(el => el.pauseAnimation())
				const [w, h] = crender.area;
				crender.delAllGraph()
				await uni.$tm.sleep(300)
				let c1 = await this.$refs.render.addGraph({
					name: 'circle',
					animationCurve: "easeInOutElastic",
					shape: {
						rx: w / 2,
						ry: 20,
						r: 6
					},
					style: {
						fill: '#0062FF'
					}
				})

				c1.animation('shape', {
						rx: 0,
						r: w
					}, true)
					.then(() => c1.animation('style', {
						rotate: -90,
						fill: 'rgba(0,98,255,1)'
					}))
					.then(() => {
						c1.animation('shape', {
							rx: w / 2,
							r: 10,
							ry: h / 2
						}, true)
						c1.animation('style', {
							rotate: 0,
							fill: 'rgba(0,98,255,0.5)'
						});
						// 克隆图形出来。

						let fg = [];
						for (let i = 0; i < 60; i++) {
							fg.push({
								name: 'circle',
								animationCurve: "easeOutBack",
								animationFrame: uni.$tm.random(20, 100),
								shape: {
									rx: uni.$tm.random(10, w - 10),
									ry: uni.$tm.random(10, h - 10),
									r: uni.$tm.random(4, 40)
								},
								style: {
									fill: `rgba(${uni.$tm.random(0,80)},0,255},${Math.random()})`,

								}
							})
						}
						crender.delGraph(c1)

						return t.$refs.render.addGraph(fg)
							.then((gflist) => uni.$tm.sleep(300).then(() => gflist))
					}).then(gflist => {
						gflist.forEach(el => {
							el['odl_rx'] = el.shape.rx;
							el.animation('shape', {
								rx: -w
							}, true)
						})
						return uni.$tm.sleep(800).then(() => gflist)
					}).then(gflist => {
						gflist.forEach(el => {

							el.animation('shape', {
								rx: el['odl_rx'],
								r: uni.$tm.random(4, 12)
							}, true)

						})
						return uni.$tm.sleep(800).then(() => gflist)
					}).then(gflist => {
						gflist.forEach(el => {
							el.animationCurve = "liner",
								el.animation('shape', {
									r: 0
								}, true)

						})
						return uni.$tm.sleep(500).then(() => {
							crender.delAllGraph();
							return uni.$tm.sleep(300)
						})

					}).then(() => {
						return t.$refs.render.addGraph({
							name: 'text',
							animationCurve: "easeOutBack",
							animationFrame: 20,
							drag:true,
							
							shape: {
								content: 'TM-VUETIFY\n可以拖动任意元素',
								position: [w / 2, h / 2],
								maxWidth:0,
							},
							style: {
								fill: 'rgba(0,98,255,1)',
								fontSize: 40,
								fontWeight: 'bold',
								opacity: 1,
							}
						}).then(tx => {
							tx.animation('shape', {
								position: [(w - tx.textWidth) / 2, h / 2],
								maxWidth:0,
							})
							return tx;
						})

					}).then(async tx => {
						
						return uni.$tm.sleep(500).then(() => {
							return t.$refs.render.addGraph({
								name: 'rect',
								animationCurve: "easeOutBack",
								animationFrame: 20,
								index: 0,
								drag:true,
								shape: {
							
									x: 0,
									y: 0,
									w: w,
									h: h
							
								},
								style: {
									fill: 'rgba(0,98,255,1)',
									gradientColor:['rgba(222, 2, 250, 1.0)','rgba(0,98,255,1)'],
									gradientParams:[0,0,w,h],
									gradientStops:[0,1],
									gradientWith:'fill'
								}
							})
						}).then((rct)=>{
							tx.index=100
							tx.animation('style', {
								fill:'white',
							}).then(()=>{
								rct.animationFrame=110;
								rct.animation('style',{
									gradientParams:[w,h,0,0]
								})
							})
							
						})
							
							

					})
			},
			async createRingAni() {
				let t = this;
				if (!this.isCanvasOk) return;
				let crender = this.$refs.render.getRender();
				this.stop = true;
				if (crender.animateAble()) {
					crender.delAllGraph()
					await uni.$tm.sleep(300)
				}
				if (crender.graphs.length > 0) {
					crender.delAllGraph()
					await uni.$tm.sleep(300)
				}
				this.stop = false;
				let ringlist = this.$refs.render.addGraph(ring(crender))
					.then(ringlist => {
						let index = 0;
						let d = true;

						function playGroup() {
							if (t.stop) return;
							let el = ringlist[index];
							if (!el) {
							 d = d ? false : true;
								index = 0;
								playGroup()
								return;
							}
							return el.animation("style", {
								lineWidth: d ? el.style.lineWidth * 2 : el.style.lineWidth / 2
							}).then(() => {
								index += 1;

								playGroup()
							})
						}
						playGroup()
					})
			}
		}
	};
</script>

```
  </CodeGroupItem>
</CodeGroup>



</content-tm>




<testtime> </testtime>