---
title: tmui的easycom安装方式
---

<content-tm>

# :heart: tmui的easycom安装方式

<span style="color:red;text-weight:bold">2021年底了，祝大家新年快乐，明年我将把它做得更好，谢谢大家的理解和支持！让我们迎接2022年的到来，今年互联网寒冬，好多企业裁员。希望来年更好，希望祖国抗住美国的制裁。</span>

***
::: tip 提醒
请仔细阅读安装步骤
***
[兼容现有老项目的vuex,安装完成后，点我查看注意事项和迁移方法>>>](/guid/special/vuex迁移.md)
***
[使用vuex，安装完成后，点我查看>>>](/guid/special/vuex.md)
:::

[[toc]]

<span style="color:red">**这里是easyncom安装方式，也就是官方uniapp推荐的，但我不推荐。注意区别手动安装方式。**</span>
#### :heavy_check_mark: 安装步骤第一步
从市场下载安装 [tm-vuetify](https://ext.dcloud.net.cn/plugin?id=5949#detail)，```然后选择示例包下载```或者```导入插件```。

---

离线下载[点我下载](https://gitee.com/LYTB/tm-ui-vuetify/releases)

### :heavy_check_mark: 第二步
示例项目中，进入项目根目录找到文件夹：tm-vuetify。<br>
如果你下载的是插件，那在插件在uni_modules->tm-vuetify下有一个tm-vuetify.zip压缩包。<br>
如果是本站下载的插件包，同样得到了一个tm-vuetify.zip压缩包<br>
解压上述插件包（如果是在示例项目中的tm-vuetify不需要解压）
### :heavy_check_mark: 第三步
复制上述tm-vuetify目录，至你的项目根目录中的根目录下！！！<br>

### :heavy_check_mark: 第四步
* 1、在main.js中配置如下配置,``` 请注意高亮部分 ```

```js{6-7}
//main.js

import Vue from 'vue'
import App from './App'

import tmVuetify from "./tm-vuetify";
Vue.use(tmVuetify)

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()

```
* 2、在```app.vue```  中如下配置请注意高亮部分
```vue{3}
<style >
	/*每个页面公共css */
	@import "./tm-vuetify/mian.min.css";
	/* 主题包 */
	@import "./tm-vuetify/scss/theme.css";
</style>

```
* 3、打开项目的根目录中```pages.json```

```vue{2,3,4}
{
	"easycom": {
		"^tm-(.*)": "@/tm-vuetify/components/tm-$1/tm-$1.vue"
	},
	...
	"pages": [ 
		...

```

### :heavy_check_mark: 第五步,可以直接使用啦.
如果你使用了此安装方法，默认是全局导入所有组件，需要关注下包的大小，
请进入[点我了解有关包体积过大的问题](/guid/start/全局导入组件.md)，查阅其中的：```前置条件```中的特别注意事项。<br>
[如何切换主题？](/guid/special/主题驱动组件.md) | 
[如何配置暗黑主题？](/guid/special/暗黑模式.md) | 
[如何使用vuex?](/guid/special/vuex.md) |
[如何合并现有项目的vuex?](/guid/special/vuex迁移.md)

---

```vue
<tm-sheet>
	<tm-signBoard></tm-signBoard>
</tm-sheet>
```

</content-tm>





<testtime> </testtime>