---
title: 如何安装tm-vuetify
---

<content-tm>

# :heart: tmui的安装

<span style="color:red;text-weight:bold">2021年底了，祝大家新年快乐，明年我将把它做得更好，谢谢大家的理解和支持！让我们迎接2022年的到来，今年互联网寒冬，好多企业裁员。希望来年更好，希望祖国抗住美国的制裁。</span>

***
::: tip 提醒
请仔细阅读安装步骤
***
[兼容现有老项目的vuex,安装完成后，点我查看注意事项和迁移方法>>>](/guid/special/vuex迁移.md)
***
[使用vuex，安装完成后，点我查看>>>](/guid/special/vuex.md)
:::

[[toc]]
<span style="color:red">**这里是手动安装方式，不是easycom方式**</span><br>
<span style="color:red">easycom安装方法略有不同，[点此查看安装](/guid/start/easyncom安装.md)</span>
#### :heavy_check_mark: 安装步骤第一步
从市场下载安装 [tm-vuetify](https://ext.dcloud.net.cn/plugin?id=5949#detail)，```然后选择示例包下载```或者```导入插件```。

---
离线下载[点我下载](https://gitee.com/LYTB/tm-ui-vuetify/releases)

#### :heavy_check_mark: 第二步
打开压缩包（示例项目），进入项目根目录找到文件夹：tm-vuetify。
#### :heavy_check_mark: 第三步
复制上述tm-vuetify目录，至你的项目根目录中。
#### :heavy_check_mark: 第四步
* 1、在main.js中配置如下配置,``` 请注意高亮部分 ```

```js{6-7}
//main.js

import Vue from 'vue'
import App from './App'

import tmVuetify from "./tm-vuetify";
Vue.use(tmVuetify)

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()

```
* 2、在```app.vue```  中如下配置请注意高亮部分<br>
你可以通过主题包theme.css中打开更多主题包。主题文件见：tm-vuetify->scss->theme目录下。
```vue{3}
<style >
	/*每个页面公共css */
	@import "./tm-vuetify/mian.min.css";
	<!-- 主题包 -->
	@import "./tm-vuetify/scss/theme.css";
</style>

```

#### :heavy_check_mark: 第五步,使用组件
<span style="color:red">新的版本从1.2.0开始，用到的组件默认需要手动导入组件才可以使用相关组件。</span><br>
拓展阅读：
[如何切换主题？](/guid/special/主题驱动组件.md) | 
[生成自己的主题色？](/guid/special/主题驱动组件.md) |
[如何配置暗黑主题？](/guid/special/暗黑模式.md) | 
[如何使用vuex?](/guid/special/vuex.md) |
[如何合并现有项目的vuex?](/guid/special/vuex迁移.md)

---
如果需要全局引入组件[请点击此处](/guid/start/全局导入组件.md)

```js
//在js需要引入组件,请注意导入组件名使用驼峰命名。
//不能这样：import tm-signBoard from "..." 这样是错误的，应该如下：
import tmSignBoard from '@/tm-vuetify/components/tm-signBoard/tm-signBoard.vue';
export default {
	components: {tmSignBoard},
	data(){
		return {
			
		}
	}
}
```
```vue
<tm-sheet>
	<tm-signBoard></tm-signBoard>
</tm-sheet>
```

</content-tm>

<content-tm>

### :bomb: 扫码预览
![H5](http://jx2d.cn/uniapp/static/qrprev.png)
![微信小程序](https://jx2d.cn/yuwuimages/weichatapp.jpg)

### :bomb: 特性预览
![自动切换主题](https://jx2d.cn/yuwuimages/themechange.gif)
![暗黑模式切换](https://jx2d.cn/yuwuimages/blacktheme.gif)
![tm-lottie动画演示图片](https://jx2d.cn/yuwuimages/lottie/ani_lottie_play.gif)
![echarts图表](https://jx2d.cn/yuwuimages/echarts.gif)
### :bomb: 预览图
![Image](/images/1@2x.jpg)
![Image](/images/2@2x.jpg)

</content-tm>






<testtime> </testtime>