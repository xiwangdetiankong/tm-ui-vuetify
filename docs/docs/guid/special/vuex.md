---
title: 内置vuex全局变量

---
<content-tm>

# 内置vuex全局变量

:::warning 注意事项

针对现有项目vuex的迁移教程及说明<br>
[如何迁移现有项目？点击查看>>](/guid/special/vuex迁移.md)

:::

***
[[toc]]

***
### 为什么要配置全局变量？
因此本组件的暗模式和主题切换需要使用vuex，但又不想破坏用户自己的vuex数据，因此我封装了自己的vuex，并且
可以轻松的与用户的vuex进行合并。

### 用户如何配置自己的vuex？（三步走）
如果你需要使用自己的vuex，而不破坏组件的vuex， 你需要如下方式配置：

:point_right:1、在你的项目根目录创建 ``` store ```文件夹。

:point_right:2、在创建的```store```目录下，创建你自己的vuex文件。

以一个自己的test例子为例。

我将在根目录下创建```store```目录，在在```store```目录下创建test.js

根目录``` -> store -> test.js ```内容如下：
```js
export default {
    state: {
       timestamp:123456 
    },
    getters: {
       timeString(state) {//时间戳转换后的时间
           var date = new Date(state.timestamp);
           var year = date.getFullYear();
           var mon  = date.getMonth()+1;
           var day  = date.getDate();
           var hours = date.getHours();
           var minu = date.getMinutes();
           var sec = date.getSeconds();
           var trMon = mon<10 ? '0'+mon : mon
           var trDay = day<10 ? '0'+day : day
           return year+'-'+trMon+'-'+trDay+" "+hours+":"+minu+":"+sec;
       }
    },
    mutations: {
        updateTime(state){
			//更新当前时间戳
            state.timestamp = Date.now()
        }
    },
    actions: {
		//通过actions提交更新数据
    	test({commit}){
    		commit('updateTime')
    	}
    }
}
```
:point_right:3、在任意页面或者组件如下使用你的vuex变量或者方法:
```js
//获取全局中timestamp的数据。
this.$tm.vx.state().test.timestamp

//使用方法,本vuex开启了namespaced命名空间。
this.$tm.vx.commit('test/updateTime')//也可以传参数this.$tm.vx.commit('updateTime',arg)
//如果你想关闭namespaced命名空间，在test,js里面写入namespaced:false,此时调用如下：
this.$tm.vx.commit('updateTime')//不包含你的文件模块名称。

//使用actions中的方法。
this.$tm.vx.actions('test/test')
//使用getters数据
this.$tm.vx.getters().test.timeString
//获取vuex对象
this.$tm.vx.getVuex() //将返回原始的store对象。

```
组件或者页面中如下使用：
```vue
<view>{{$tm.vx.state().test.timestamp}}</view>
```

</content-tm>

<testtime> </testtime>