---
title: vuex迁移

---
<content-tm>

# 老项目中自己的vuex迁移
***
[[toc]]

***


### 老项目已有vuex如何与本主题合并？

:point_right:1、在你的项目根目录创建 ``` store ```文件夹。

:point_right:2、在创建的```store```目录下，复制你已有的vuex文件到 store文件夹中。

**进入你已有项目中的main.js**<br>

下方是个错误的示例，也是大多数人常用的示例如下：
```js
//main.js
import store from "./store/index.js";
//或者
import store from "./store";

Vue.prototype.$store = store

```
**<span style="color:green">正确的做法改成如下:</span>**
```js

//是的把你的store配置全部干掉，因为我的组件已经为你做了配置。
//import store from "./store/index.js";
//import store from "./store";
//Vue.prototype.$store = store

```

**这里重要的一步**
- 如果你的vuex文件本身是使用模块化作为vuex文件导入的。那么你跳过第1点，直接看第2点。
- 如果你是一个/store/index.js文件请看第1点。

**第1点打开你的主单文件store/index.js**<br>
```js
//store/index.js你的主vuex文件。下方是你的文件格式：这是一个错误的示范。
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
var store = new Vuex.Store({  
		state: {  
		  ...
		},  
		mutations: {  
		  ...
		},
		...
})

export default store;

```

**你需要将上方的旧格式改成以下方式,请注意需要你注释掉相关vuex,和vue的导入都不需要了**<br>
```js
//store/index.js你的主vuex文件。这是一个正确的示范。
export default {  
	state: {  
	  ...
	},  
	mutations: {  
	  ...
	},
	...
}

```
**第2点你已经是模块化vuex方式，并且你的index.js是主模块，还有其它子模块vuex文件在同一目录**<br>
- 如果你的根index.js有自己的一级属性和方法，那么你需要像第1点那样创建个子模块，然后把根属性和方法放入其中
把，命名空间关闭，这样你这个子模块的方法就可以成为一级直接this.$tm.vx.commit('方法名')直接使用。
- 如果你的根index.js没有自己的属性和方法，只是负责导入其它子模块，那么你需要将其它子模块js全部放入store目录
展开作为平级。不要再store下创建地子目录即可。使用方法遵循模块化调用如:this.$tm.vx.commit('test/updateTime')
test为模块名，updateTime为方法名。<br>

**第3点：你有自己的vuex模块,同样需要像第1，2点一样处理文件。如果你不想用this.$tm.vx或者想兼容使用this.$store这样的方式调用？**<br>
你需要找到插件目录tm-vuetify->index.js 第100行（接到文件末尾），把注释打开即可。这样你就可以继续使用this.$store这样的方式。

:point_right:3、在任意页面或者组件如下使用你的vuex变量或者方法:
```js
//获取全局中timestamp的数据。
this.$tm.vx.state().test.timestamp

//使用方法,本vuex开启了namespaced命名空间。
this.$tm.vx.commit('test/updateTime')//也可以传参数this.$tm.vx.commit('updateTime',arg)
//如果你想关闭namespaced命名空间，在test,js里面写入namespaced:false,此时调用如下：
this.$tm.vx.commit('updateTime')//不包含你的文件模块名称。

//使用actions中的方法。
this.$tm.vx.actions('test/updateTime')
//使用getters数据
this.$tm.vx.getters().test.timeString
//获取vuex对象
this.$tm.vx.getVuex() //将返回原始的store对象。

```
组件或者页面中如下使用：
```vue
<view>{{$tm.vx.state().test.timestamp}}</view>
```

</content-tm>

<testtime> </testtime>