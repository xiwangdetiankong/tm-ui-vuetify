---
title: mixin混淆函数
---
<content-tm>

### :pray: mixin混淆函数


:::danger 注意
如何使用混淆函数：在vue页面或者组件中的上下文下。使用```this.函数名```来使用。
:::


| 函数名称 | 参数名称 | 参数的默认值 | 是否必填 | 返回说明 | 作用 |
| :-----: | :--: | :---: | :---: | :--- | :--- |
| $TestColor | color\<String\> | '' | 是 | 返回：{theme:true,color:color} | 用来检测提供的值，是主题色名称还是颜色值 |
| $TestUnitr | n\<String\|Number\> | '' | 是 | 返回：{type:'number|string',value:值} | 用来检测提供的字符串是数值还是带单位的数值，如5px,10vw,50%这样的宽度变量值 |
| $Querey | clsaaName(提供的类名),t(提供上下文对象，一般为this) | '' | 是 | 返回：查询的节点宽高信息 | 查询节点宽高信息 |

</content-tm>

<testtime> </testtime>
