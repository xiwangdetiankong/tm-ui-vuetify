---
home: true
heroImage: /images/logo_great.png
heroText: tm-vuetify
tagline: UNI-APP通用组件库、JS工具包、CSS库,vuex，lottie,canvas渲染引擎，主题，暗黑模式切换,齐全的组件库，跨端使用！
actions:
  - text: 快速上手
    link: /guid/start/安装.md
    type: primary
  - text: 项目简介
    link: /guid/project/介绍.md
    type: secondary

footer: MIT Licensed | Copyright © 2021 tmzdy-tm-vuetify tmui | https://jx2d.cn
---



<br>

<home-bg></home-bg>

<hr>

<span style="text-align:center;font-size:1.5rem;display:block;padding:1rem;color: #5b7aeb;">tm-vuetify跨端组件库(微信小程序，支付宝小程序，头条小程序等等各家小程序)，app,H5均支持，一套UI库走遍所有端！</span>

<br>
<br>

---

<div style="text-align:center;font-size:12px;padding:24px 0">
<a href="https://beian.miit.gov.cn/#/Integrated/index" target="_blank"> 赣ICP备16005577号-3 </a>
</div>


