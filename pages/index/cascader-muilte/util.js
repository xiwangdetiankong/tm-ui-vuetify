export const findeParent = function(parent){
	if(!parent) return null;
	if(parent.$options?.name=='cascaderMuilte'){
		return parent
	}
	return findeParent(parent?.$parent)
}
//平铺节点
export const flatNodes = (data) => {
	return data.reduce((res, node) => {
		res = Array.isArray(node.children)?res.concat(flatNodes(node.children)):res.concat(node);
		return res;
	}, []);
};

//返回 一个节点从父到子的路径id组。
export const getNodeRouter = function (list=[],id='',prentId=[],idmap='id'){
	let p = [];
	if(typeof prentId =='undefined'){
		prentId=[];
	}
	if(!Array.isArray(id)){
		id = [id]
	}
	let arr = Array.from(prentId)
	for (let i = 0, len = list.length; i < len; i++) {
		arr.push(list[i][idmap])
		if (list[i][idmap] == id[0]) {
			return arr
		}
		let children = list[i].children
		if (children && children.length) {
			let result = getNodeRouter(children, id, arr,idmap)
			if (result) return result
		}
		arr.pop()
	}
	return null
}

// 是否选中
export const isSameChecked = (data,value) => {
	return new Set(data).has(value)
};
// 求出数组总共有多少级。
export  function getArrayLayer(arr, attr, index = 1) {
  let newIndex = index;
  for (const iterator of arr) {
	let tempIndex = index
	if (iterator[attr]) {
	  tempIndex = getArrayLayer(iterator[attr], attr, index + 1)
	  if (tempIndex > newIndex) {
		newIndex = tempIndex
	  }
	}
  }
  return newIndex
}

 export  function menuListFilter (arr, name,idmap='id') {
	let menu=null;
	for(let i=0;i<arr.length;i++){
		let item = arr[i]
		if(item[idmap] == name) {
			menu = item;
			break;
			return menu;
		}else{
			if(item?.children){
				let menus = menuListFilter(item.children, name)
				if(menus){
					menu = menus;
				}
			}
		}
		
	}
	return menu
}


export  function getNodeParent(data2, nodeId2,idmap='value') {
	var arrRes = [];
	if (data2.length == 0) {
		if (!!nodeId2) {
			arrRes.unshift(nodeId2);
		}
		return arrRes;
	}
	let rev = (data, nodeId) => {
		for (var i = 0, length = data.length; i < length; i++) {
			let node = data[i];
			if (node[idmap] == nodeId) {
				arrRes.unshift(nodeId);
				rev(data2, node.City);
				break;
			}
			else {
				if (!!node.children) {
					rev(node.children, nodeId);
				}
			}
		}
		return arrRes;
	};
	arrRes = rev(data2, nodeId2);
	return arrRes;
}
