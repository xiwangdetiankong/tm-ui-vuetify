const jsonData = {
	'1_1': {
		xAxis: {
			type: 'category',
			data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
		},
		yAxis: {
			type: 'value'
		},
		series: [{
			data: [150, 230, 224, 218, 135, 147, 260],
			type: 'line'
		}]
	},
	'1_2': {

		tooltip: {
			trigger: 'axis',
			axisPointer: {
				type: 'cross',
				label: {
					backgroundColor: '#6a7985'
				}
			}
		},
		legend: {
			data: ['Email', 'Union Ads', 'Video Ads', 'Direct', 'Search Engine']
		},
		toolbox: {
			feature: {
				saveAsImage: {}
			}
		},
		grid: {
			left: '3%',
			right: '4%',
			bottom: '3%',
			containLabel: true
		},
		xAxis: [{
			type: 'category',
			boundaryGap: false,
			data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
		}],
		yAxis: [{
			type: 'value'
		}],
		series: [{
				name: 'Email',
				type: 'line',
				stack: 'Total',
				areaStyle: {},
				emphasis: {
					focus: 'series'
				},
				data: [120, 132, 101, 134, 90, 230, 210]
			},
			{
				name: 'Union Ads',
				type: 'line',
				stack: 'Total',
				areaStyle: {},
				emphasis: {
					focus: 'series'
				},
				data: [220, 182, 191, 234, 290, 330, 310]
			},
			{
				name: 'Video Ads',
				type: 'line',
				stack: 'Total',
				areaStyle: {},
				emphasis: {
					focus: 'series'
				},
				data: [150, 232, 201, 154, 190, 330, 410]
			},
			{
				name: 'Direct',
				type: 'line',
				stack: 'Total',
				areaStyle: {},
				emphasis: {
					focus: 'series'
				},
				data: [320, 332, 301, 334, 390, 330, 320]
			},
			{
				name: 'Search Engine',
				type: 'line',
				stack: 'Total',
				label: {
					show: true,
					position: 'top'
				},
				areaStyle: {},
				emphasis: {
					focus: 'series'
				},
				data: [820, 932, 901, 934, 1290, 1330, 1320]
			}
		]
	},
	'2_1': {
		xAxis: {
			type: 'category',
			data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
		},
		yAxis: {
			type: 'value'
		},
		series: [{
			data: [120, 200, 150, 80, 70, 110, 130],
			type: 'bar'
		}]
	},
	'2_2': {
		dataset: {
			source: [
				['score', 'amount', 'product'],
				[89.3, 58212, 'Matcha Latte'],
				[57.1, 78254, 'Milk Tea'],
				[74.4, 41032, 'Cheese Cocoa'],
				[50.1, 12755, 'Cheese Brownie'],
				[89.7, 20145, 'Matcha Cocoa'],
				[68.1, 79146, 'Tea'],
				[19.6, 91852, 'Orange Juice'],
				[10.6, 101852, 'Lemon Juice'],
				[32.7, 20112, 'Walnut Brownie']
			]
		},
		grid: {
			containLabel: true
		},
		xAxis: {
			name: 'amount'
		},
		yAxis: {
			type: 'category'
		},
		visualMap: {
			orient: 'horizontal',
			left: 'center',
			min: 10,
			max: 100,
			text: ['High Score', 'Low Score'],
			// Map the score column to color
			dimension: 0,
			inRange: {
				color: ['#65B581', '#FFCE34', '#FD665F']
			}
		},
		series: [{
			type: 'bar',
			encode: {
				// Map the "amount" column to X axis.
				x: 'amount',
				// Map the "product" column to Y axis
				y: 'product'
			}
		}]
	},
	'3_1': {
		title: {

			subtext: 'Fake Data',
			left: 'center'
		},
		tooltip: {
			trigger: 'item'
		},
		legend: {
			orient: 'vertical',
			left: 'left'
		},
		series: [{
			name: 'Access From',
			type: 'pie',
			radius: '50%',
			data: [{
					value: 1048,
					name: 'Search Engine'
				},
				{
					value: 735,
					name: 'Direct'
				},
				{
					value: 580,
					name: 'Email'
				},
				{
					value: 484,
					name: 'Union Ads'
				},
				{
					value: 300,
					name: 'Video Ads'
				}
			],
			emphasis: {
				itemStyle: {
					shadowBlur: 10,
					shadowOffsetX: 0,
					shadowColor: 'rgba(0, 0, 0, 0.5)'
				}
			}
		}]
	},
	'3_2': {
		legend: {
			top: 'bottom'
		},
		toolbox: {
			show: true,
			feature: {
				mark: {
					show: true
				},
				dataView: {
					show: true,
					readOnly: false
				},
				restore: {
					show: true
				},
				saveAsImage: {
					show: true
				}
			}
		},
		series: [{
			name: 'Nightingale Chart',
			type: 'pie',
			radius: [50, 120],
			center: ['50%', '50%'],
			roseType: 'area',
			itemStyle: {
				borderRadius: 8
			},
			data: [{
					value: 40,
					name: 'rose 1'
				},
				{
					value: 38,
					name: 'rose 2'
				},
				{
					value: 32,
					name: 'rose 3'
				},
				{
					value: 30,
					name: 'rose 4'
				},
				{
					value: 28,
					name: 'rose 5'
				},
				{
					value: 26,
					name: 'rose 6'
				},
				{
					value: 22,
					name: 'rose 7'
				},
				{
					value: 18,
					name: 'rose 8'
				}
			]
		}]
	},
	'4_1':{
  tooltip: {
    formatter: '{a} <br/>{b} : {c}%'
  },
  series: [
    {
      name: 'Pressure',
      type: 'gauge',
      detail: {
        formatter: '{value}'
      },
      data: [
        {
          value: 50,
          name: 'SCORE'
        }
      ]
    }
  ]
},
'4_2':{
  series: [
    {
      type: 'gauge',
      axisLine: {
        lineStyle: {
          width: 20,
          color: [
            [0.3, '#67e0e3'],
            [0.7, '#37a2da'],
            [1, '#fd666d']
          ]
        }
      },
	   radius:'90%',
      pointer: {
        itemStyle: {
          color: 'auto'
        }
      },
      axisTick: {
        distance: -20,
        length: 8,
        lineStyle: {
          color: '#fff',
          width: 2
        }
      },
      splitLine: {
        distance: -20,
        length: 20,
        lineStyle: {
          color: '#fff',
          width: 4
        }
      },
      axisLabel: {
        color: 'auto',
        distance: 40,
        fontSize: 12
      },
      detail: {
        valueAnimation: true,
        formatter: '{value} km/h',
        color: 'auto',
		 offsetCenter:[0,'60%'],
		fontSize:18
      },
      data: [
        {
          value: 70
        }
      ]
    }
  ]
}
}
export default jsonData;
